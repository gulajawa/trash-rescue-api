/**
 * Created by muharizals on 20/02/2017.
 */


const TPS = require('../models/TpsModel'),
      Sensor = require('../models/Sensor'),
      Helper = require('../helper/ResponseHelper'),
      Constant = require('../util/constants'),
      Util = require('../util/util'),
      LocationHelper = require('../helper/LocationHelper'),
      SocketHelper = require('../helper/SocketIOHelper'),
      config = require('../../config'),
      PetugasController = require('./PetugasController');



exports.setIo = function (soio) {
   SocketHelper.setIo(soio);
};

function findTpsToEmit(container) {
    TPS.findOne({'container.sensor':container.sensor},function (err,tps) {
       if(!err && !Util.isEmpty(tps)){
            SocketHelper.notifyTPSUpdate(tps);
       }
    });
}

function cekIsContainerFull(data,container,nearTps) {
    let sensor_1 = data.jarak_sensor_1 - config.YDistance;
    let sensor_2 = data.jarak_sensor_2 - config.YDistance;

    let totaljarak = (sensor_1 + sensor_2) / 2;

    let setengah = container.tinggi_awal/2;
    let seperempat = container.tinggi_awal/4;
    let seperdelapan = container.tinggi_awal/8;

    let dif_sensor = container.tinggi_awal - totaljarak;

    if(dif_sensor >= setengah && dif_sensor <(setengah+seperdelapan)){
        container.status = "HAMPIR_PENUH";
        container.jam_penuh = undefined;
    }

    if(dif_sensor >= seperempat && dif_sensor < setengah){
        container.status = "SETENGAH_PENUH";
        container.jam_penuh = undefined;
    }

    if(dif_sensor <= seperdelapan ){
        container.status = "KOSONG";
        container.jam_penuh = undefined;
        PetugasController.getPetugasToSendNotifApaSudahDiambil(nearTps,container);
    }

    if(dif_sensor >= (container.tinggi_awal-seperdelapan) ){
        container.status = "PENUH";
        container.jam_penuh = new  Date();
        //ini disi isi jika penuh kirim ke petugas berdasarkan ???
        PetugasController.getPetugasToSendNotif(nearTps,container);
    }

    /*
    let dif_sensor1 = container.tinggi_awal - sensor_1;
    let dif_sensor2 = container.tinggi_awal - sensor_2;



    if((dif_sensor1 >= setengah && dif_sensor1 <(setengah+seperdelapan)) && (dif_sensor2 >= setengah && dif_sensor2 < (setengah+seperdelapan))){
        container.status = "HAMPIR_PENUH";
        container.jam_penuh = undefined;
    }
    if((dif_sensor1 >= seperempat && dif_sensor1 < setengah) && (dif_sensor2 >= seperempat && dif_sensor2 < setengah )){
        container.status = "SETENGAH_PENUH";
        container.jam_penuh = undefined;
    }
    if(dif_sensor1 >= (container.tinggi_awal-seperdelapan) && (dif_sensor2 <= setengah) ){
        container.status = "SETENGAH_PENUH";
        container.jam_penuh = undefined;
    }

    if(dif_sensor2 >= (container.tinggi_awal-seperdelapan) && (dif_sensor1 <= setengah) ){
        container.status = "SETENGAH_PENUH";
        container.jam_penuh = undefined;
    }

    if(dif_sensor1 <= seperdelapan && dif_sensor2 <= seperdelapan){
        container.status = "KOSONG";
        container.jam_penuh = undefined;
        PetugasController.getPetugasToSendNotifApaSudahDiambil(nearTps,container);
    }
    if(dif_sensor1 >= (container.tinggi_awal-seperdelapan) && dif_sensor2 >= container.tinggi_awal-seperdelapan){
        container.status = "PENUH";
        container.jam_penuh = new  Date();
        //ini disi isi jika penuh kirim ke petugas berdasarkan ???
        PetugasController.getPetugasToSendNotif(nearTps,container);
    }
    */

    TPS.findOne({'container.sensor':container.sensor},'_id',function (err,hasil) {
       if(err){

       }else{
           if(hasil){
               TPS.removeContainerSensor(container.sensor).exec(function (err) {
                   if(!err){
                       TPS.updateTpsContainer(nearTps.id,container.id,container.status,container.jam_penuh,container.sensor).exec(function (err2) {
                           if(!err2){
                                findTpsToEmit(container);
                           }
                       });
                   }
               });
           }else{
               TPS.updateTpsContainer(nearTps.id,container.id,container.status,container.jam_penuh,container.sensor).exec(function (err2) {
                   if(!err2){
                        findTpsToEmit(container);
                   }
               });
           }

       }
    });

}


function doThinkPutSensors(i,nearTps,data) {

    if(i == nearTps.container.length){
        return;
    }
    if(!nearTps.container[i].sensor){
        nearTps.container[i].sensor = data.id;
        cekIsContainerFull(data,nearTps.container[i],nearTps);
        return;
    }
    if(nearTps.container[i].sensor != data.id){
        Sensor.findById(nearTps.container[i].sensor,function (err,cursensor) {
            if(err){
                nearTps.container[i].sensor = data.id;
                cekIsContainerFull(data,nearTps.container[i],nearTps);
            }
            else if(Util.isEmpty(cursensor)){
                nearTps.container[i].sensor = data.id;
                cekIsContainerFull(data,nearTps.container[i],nearTps);
            }
            else{
                let now = new Date();

                if(now.getTime() - cursensor.updatedAt.getTime() >= config.intervalSensor*60*1000){
                    //sudah lebih dari 30 menit
                    nearTps.container[i].sensor = data.id;
                    cekIsContainerFull(data,nearTps.container[i],nearTps);
                }
                else{
                    //belum ada 30 menit
                    i = i+1;
                    doThinkPutSensors(i,nearTps,data);
                }

            }
        });
    }else{
        cekIsContainerFull(data,nearTps.container[i],nearTps);
    }

}

exports.updateTpsSensor = function (id) {
    Sensor.findById(id,function (err,data) {
        if(!err){
            let limit = 1,maxDistance=config.maxDistance;

            TPS.find({
              location:{
                  $near:data.location,
                  $maxDistance: maxDistance
              }
            }).limit(limit).exec(function (err2,results) {
               if(err2){
                   console.log(err2);
               }
               if(!err2){
                    if(!Util.isEmpty(results)){
                        doThinkPutSensors(0,results[0],data);
                    }
               }
            });
        }
    });

};


exports.getListTps = function (req,res) {
    let query= {};
    if(req.params.hasOwnProperty('wilayah')){
        query.wilayah = req.params.wilayah;
    }
    TPS.find(query,{__v:0}).populate('container.sensor wilayah','-__v  -location').exec(function (err,data) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }

        if(Util.validateParameter(req,["limit","offset"])){
            let offset = req.params.offset,limit = req.params.limit,selisih = offset+limit;
            return Helper.responseHelper(res,false,false,data.slice(offset,selisih));
        }
        return Helper.responseHelper(res,false,false,data);
    });

};

exports.getSingleTPS = function (req,res) {
    TPS.findById({_id:req.params.id},{__v:0}).populate('container.sensor wilayah').exec(function(err,data) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }else{
            return Helper.responseHelper(res,false,false,data);
        }
    });
};

function updateTps(req,res,body) {
    TPS.findOneAndUpdate({_id:req.params.id},body,function (err,result) {
        if(err){
            return Helper.responseHelper(res,true,Constant.INVALID_TPS_ID);
        }else{
            if(Util.isEmpty(result)){
                return Helper.responseHelper(res,true,Constant.INVALID_TPS_ID);
            }
            TPS.findById(req.params.id,'-__v').
                populate('wilayah').
                exec(function (err,hasil) {
                    if(err){
                        return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                    }
                    return Helper.responseHelper(res,false,false,hasil);
                });
        }
    });
}

exports.updateSingleTPS = function (req,res) {
    if(!Util.validateParameter(req,["id"])){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
    let body = {};
    if(req.params.hasOwnProperty('name')){
        body.name = req.params.name;
    }
    if(req.params.hasOwnProperty('wilayah')){
        body.wilayah = req.params.wilayah;
    }
    if(req.params.hasOwnProperty('latitude') && req.params.hasOwnProperty('longitude')){
        LocationHelper.getGeocoder().reverse({lat:req.params.latitude,lon:req.params.longitude},function (err,results) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }
            if(Util.isEmpty(results)){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ADDRESS);
            }
            body.location = [req.params.longitude,req.params.latitude];
            body.address = results[0].formattedAddress;
            body.kecamatan = results[0].administrativeLevels.level3short;
            return updateTps(req,res,body);
        });
    }else{
        return updateTps(req,res,body);
    }
};

exports.deleteSingleTps = function (req,res) {
    if(!Util.validateParameter(req,["id"])){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }

    TPS.findOne({_id:req.params.id},function (err,result) {
       if(err){
           return Helper.responseHelper(res,true,Constant.INVALID_TPS_ID);
       }
       if(Util.isEmpty(result)){
           return Helper.responseHelper(res,true,Constant.INVALID_TPS_ID);
       }

       result.remove(function (err2,tps) {
          if(err2){
              return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
          }
          return Helper.responseHelper(res,false,false,tps);
       });
    });


};

exports.deleteAllTPS = function (req,res) {
    let query= {};
    if(req.params.hasOwnProperty('wilayah')){
        query.wilayah = req.params.wilayah;
    }
    TPS.remove(query,function (err) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }else{
            return Helper.responseHelper(res,false);
        }
    });
};


exports.createNewTPS = function (req,res) {
    if(!Util.validateParameter(req,["name","latitude","longitude","container","wilayah"])){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }

    if(isNaN(req.params.container)){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }

    LocationHelper.getGeocoder().reverse({lat:req.params.latitude,lon:req.params.longitude},function (err,results) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        if(Util.isEmpty(results)){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ADDRESS);
        }


        let body = {};
        body.name = req.params.name;
        body.location = [req.params.longitude,req.params.latitude];

        body.address = results[0].formattedAddress;
        body.kecamatan = results[0].administrativeLevels.level3short;
        body.wilayah = req.params.wilayah;

        let newTps = new TPS(body);

        if(req.params.container > 0){
            let container = Util.getInitContainer();
            for (let i=0;i<req.params.container;i++){
                newTps.container.push(container);
            }
        }

        newTps.save(function (err) {
               if(err){
                   return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
               }else{
                   TPS.findOne({_id:newTps.id}).
                       populate('wilayah').
                       exec(function (err2,tps) {
                           if(err2){
                               return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                           }
                           return Helper.responseHelper(res,false,false,tps,201);
                       });

               }
        });


    });
};

exports.addContainer=function (req,res) {
    if(!Util.validateParameter(req,["id","jumlah"])){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
    if(isNaN(req.params.jumlah)){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
    TPS.findById(req.params.id,'container',function (err,data) {
        if(err){
            return Helper.responseHelper(res,true,Constant.INVALID_TPS_ID,false,400);
        }
        if(Util.isEmpty(data)){
            return Helper.responseHelper(res,true,Constant.INVALID_TPS_ID,false,400);
        }

        if(req.params.jumlah > 0){
            let container = Util.getInitContainer();
            if(req.params.hasOwnProperty('tinggi_awal')){
                if(isNaN(req.params.tinggi_awal)){
                    return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                }
                container.tinggi_awal = req.params.tinggi_awal;
            }
            if(req.params.hasOwnProperty('ukuran')){
                container.ukuran = req.params.ukuran;
            }
            for (let i=0;i<req.params.jumlah;i++){
                data.container.push(container);
            }
            data.save(function (err) {
                if(err){
                    return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                }else{
                    return Helper.responseHelper(res,false,false,data,201);
                }
            });
        }else{
            return Helper.responseHelper(res,false,false,data,201);
        }

    });
};

exports.updateTinggiSingleContainer= function (req,res) {
    if(!Util.validateParameter(req,["id","container_id","tinggi_awal"])){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
    if(isNaN(req.params.tinggi_awal)){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }

    TPS.updateTinggiContainer(req.params.id,req.params.container_id,req.params.tinggi_awal).exec(function (err) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }

        TPS.findById(req.params.id,'container',function (err,data) {
            if(err){
                return Helper.responseHelper(res,true,Constant.INVALID_TPS_ID,false,400);
            }
            if(Util.isEmpty(data)){
                return Helper.responseHelper(res,true,Constant.INVALID_TPS_ID,false,400);
            }
            return Helper.responseHelper(res,false,false,data);
        });


    });


};


exports.removeAllContainer = function (req,res) {
   if(!Util.validateParameter(req,["id"])){
       return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
   }

   TPS.findById(req.params.id,'container',function (err,data) {
       if(err){
           return Helper.responseHelper(res,true,Constant.INVALID_TPS_ID,false,400);
       }
       if(Util.isEmpty(data)){
           return Helper.responseHelper(res,true,Constant.INVALID_TPS_ID,false,400);
       }
       data.container = [];
       data.save(function (err) {
           if(err){
               console.log(err);
               return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
           }else{
               return Helper.responseHelper(res,false,false,data);
           }
       });
   });
};

exports.removeSingleContainer = function (req,res) {
    if(!Util.validateParameter(req,["id","container_id"])){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }

    TPS.findById(req.params.id,'container',function (err,data) {
        if(err){
            return Helper.responseHelper(res,true,Constant.INVALID_TPS_ID,false,400);
        }
        if(Util.isEmpty(data)){
            return Helper.responseHelper(res,true,Constant.INVALID_TPS_ID,false,400);
        }
        if(data.container.length > 0){
            data.container.pull(req.params.container_id);
            data.save(function (err) {
                if(err){
                    console.log(err);
                    return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                }else{
                    return Helper.responseHelper(res,false,false,data);
                }
            });

        }else{
            return Helper.responseHelper(res,false,false,data);
        }
    });
};

