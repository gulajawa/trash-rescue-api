/**
 * Created by muharizals on 12/05/2017.
 */
const Wilayah  = require('../models/Wilayah'),
    Helper = require('../helper/ResponseHelper'),
    Util = require('../util/util'),
    Constant = require('../util/constants');


exports.getAll=function(req,res){
  Wilayah.find({},function (err,wilayah) {
     if(err){
         return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
     }
      return Helper.responseHelper(res,false,false,wilayah);
  });
};

exports.deleteWilayah=function (req,res) {
    if(Util.validateParameter(req,["id"])){
        Wilayah.findOneAndRemove({_id:req.params.id},function (err,result) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_USER);
            }
            else{
                return Helper.responseHelper(res,false,result);
            }
        });
    }else{
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
};

exports.deleteAllWilayah=function (req,res) {
    Wilayah.remove({},function (err) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }else{
            return Helper.responseHelper(res,false);
        }
    });
};

exports.updateWilayah=function(req,res){
    if(Util.validateParameter(req,["id","name"])){
        Wilayah.findOneAndUpdate({_id:req.params.id},req.params,function (err,result) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }else{
                Wilayah.findById(req.params.id).exec(function (err,wilayah) {
                    if(err){
                        return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                    }
                    return Helper.responseHelper(res,false,false,wilayah);
                });
            }
        });
    }else{
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
};

exports.createNewWilayah=function (req,res) {
    if(Util.validateParameter(req,["name"])){
        let baru = new Wilayah(req.params);
        baru.save(function (err) {
           if(err){
               return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
           }
           return Helper.responseHelper(res,false,false,baru,201);
        });
    }else{
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
};
