/**
 * Created by muharizals on 08/02/2017.
 */

const Sensor = require('../models/Sensor'),
      TPSModel = require('../models/TpsModel'),
      Helper = require('../helper/ResponseHelper'),
      Constant = require('../util/constants'),
      Util = require('../util/util'),
      LocationHelper = require('../helper/LocationHelper'),
      SocketHelper = require('../helper/SocketIOHelper'),
      TPS =  require('./TPSController');



exports.setIo = function (soio) {
    SocketHelper.setIo(soio);
    TPS.setIo(soio);
};

exports.createNewSensorV2 = function (req,res) {
  if(!Util.validateParameter(req,["tps_id","wilayah"])){
      return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
  }
  TPSModel.findOne({_id:req.params.tps_id},'location',function (err,result) {
      if(err){
          return Helper.responseHelper(res,true,Constant.INVALID_TPS_ID);
      }
      let body = {};
      body.wilayah = req.params.wilayah;
      body.api_key = Util.getUniqueApiKey(30);
      body.location = result.location;
      let newsensor = new Sensor(body);
      newsensor.save(function (err) {
          if(err){
              return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
          }else{
              Sensor.findOne({_id:newsensor.id}).
              populate('wilayah').
              exec(function (err3,sensor) {
                  if(err3){
                      return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                  }
                  return Helper.responseHelper(res,false,false,sensor,201);
              });
          }
      });
  });
};

exports.createNewSensor=function (req,res) {

    if(Util.validateParameter(req,["address","wilayah"])){
        LocationHelper.getGeocoder().geocode(req.params.address,function (err,result) {
           if(err){
               return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
           }else{
                if(Util.isEmpty(result)){
                    return Helper.responseHelper(res,true,Constant.UNKNOWN_ADDRESS);
                }else{
                    let body = {};
                    body.wilayah = req.params.wilayah;
                    body.api_key = Util.getUniqueApiKey(30);
                    body.location = [result[0].longitude,result[0].latitude];
                    let newsensor = new Sensor(body);
                    newsensor.save(function (err) {
                        if(err){
                            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                        }else{
                            Sensor.findOne({_id:newsensor.id}).
                                populate('wilayah').
                                exec(function (err3,sensor) {
                                    if(err3){
                                        return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                                    }
                                    return Helper.responseHelper(res,false,false,sensor,201);
                                });
                        }
                    });
                }
           }
        });
    }else{
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
};

exports.getSensor =function (req,res) {
    let query= {};
    if(req.params.hasOwnProperty('wilayah')){
        query.wilayah = req.params.wilayah;
    }
    Sensor.find(query,{__v:0}).
        populate('wilayah').
        exec(function (err,data) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }else{
                if(Util.validateParameter(req,["limit","offset"])){
                    let offset = req.params.offset,limit = req.params.limit,selisih = offset+limit;
                    return Helper.responseHelper(res,false,false,data.slice(offset,selisih));
                }else{
                    return Helper.responseHelper(res,false,false,data);
                }
            }
        });
};

exports.getSingleSensor = function (req,res) {
    Sensor.findById({_id:req.params.sensor_id},{__v:0},function(err,data) {
       if(err){
           return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
       }else{
           return Helper.responseHelper(res,false,false,data);
       }
    });
};

exports.deleteSingleSensor = function (req,res) {
    Sensor.findOne({api_key:req.params.api_key},function (err,result) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }else{
            result.remove(function (er2,sens) {
               if(er2){
                   return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
               }else{
                   return Helper.responseHelper(res,false,false,sens);
               }
            });
        }
    });
};

exports.deleteAllSensor = function (req,res) {
    let query= {};
    if(req.params.hasOwnProperty('wilayah')){
        query.wilayah = req.params.wilayah;
    }
  Sensor.remove(query,function (err) {
     if(err){
         return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
     }else{
         return Helper.responseHelper(res,false);
     }
  });
};

function updateSensor(req,body,res) {
    Sensor.findOneAndUpdate({api_key:req.params.api_key},body,function (err,result) {
       if(err){
           return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
       }else{
           Sensor.findOne({api_key:req.params.api_key},function (err,sensor) {
              if(!err){
                  if(!Util.isEmpty(sensor)){
                      SocketHelper.notiftySensorUpdate(sensor);
                  }
              }
           });
           TPS.updateTpsSensor(result.id);
           return Helper.responseHelper(res,false,false);
       }
    });
}

exports.updateSingleSensor = function (req,res) {

    let body = {};
    if(req.params.hasOwnProperty('jarak_sensor_1')){
        body.jarak_sensor_1 = req.params.jarak_sensor_1;
    }
    if(req.params.hasOwnProperty('jarak_sensor_2')){
        body.jarak_sensor_2 = req.params.jarak_sensor_2;
    }

    if(req.params.hasOwnProperty('wilayah')){
        body.wilayah = req.params.wilayah;
    }

    if(Util.validateParameter(req,["lat","lng"])){
        body.location = [req.params.lng,req.params.lat];
        return updateSensor(req,body,res);
    }else{
        return updateSensor(req,body,res);
    }

};

