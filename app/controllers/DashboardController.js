/**
 * Created by muharizals on 18/04/2017.
 */

const Helper = require('../helper/ResponseHelper'),
      Constant = require('../util/constants'),
      Admin = require('../models/Admin'),
      Petugas = require('../models/Petugas'),
      Sensor = require('../models/Sensor'),
      Util = require('../util/util'),
      TPS = require('../models/TpsModel');

function countSensorActive(req,res,dataresponse,query) {
    Sensor.countActiveSensor(query,function (err,result) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        if(Util.isEmpty(result)){
            dataresponse.sensor_active = 0;
        }else{
            dataresponse.sensor_active = result[0].count;
        }

        dataresponse.sensor_inactive = dataresponse.sensor - dataresponse.sensor_active;
        return Helper.responseHelper(res,false,false,dataresponse);
    });
}


function countHampirPenuh(req,res,dataresponse,query) {
    let query2 = {
        'container.status':'HAMPIR_PENUH'
    };
    if(query.wilayah){
        query2.wilayah = query.wilayah;
    }
    TPS.count(query2,function (err,count) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        dataresponse.hampir_penuh = count;
        return countSensorActive(req,res,dataresponse,query);
    });
}


function countSetangahPenuh(req,res,dataresponse,query) {
    let query2 = {
        'container.status':'SETENGAH_PENUH'
    };
    if(query.wilayah){
        query2.wilayah = query.wilayah;
    }
    TPS.count(query2,function (err,count) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        dataresponse.setengah_penuh = count;
        return countHampirPenuh(req,res,dataresponse,query);
    });
}


function countTidakDiketahui(req,res,dataresponse,query) {
    let query2 = {
        'container.status':'TIDAK_DIKETAHUI'
    };
    if(query.wilayah){
        query2.wilayah = query.wilayah;
    }
    TPS.count(query2,function (err,count) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        dataresponse.tidak_diketahui = count;
        return countSetangahPenuh(req,res,dataresponse,query);
    });
}

function countKosong(req,res,dataresponse,query) {
    let query2 = {
        'container.status':'KOSONG'
    };
    if(query.wilayah){
        query2.wilayah = query.wilayah;
    }
    TPS.count(query2,function (err,kosongcount) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        dataresponse.kosong = kosongcount;
        return countTidakDiketahui(req,res,dataresponse,query);
    });
}

function countPenuh(req,res,dataresponse,query) {
    let query2 = {
        'container.status':'PENUH'
    };
    if(query.wilayah){
        query2.wilayah = query.wilayah;
    }
    TPS.count(query2,function (err,penuhcount) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        dataresponse.penuh = penuhcount;
        return countKosong(req,res,dataresponse,query);
    });
}

function countTps(req,res,dataresponse,query) {
    TPS.count(query,function (err,tpscount) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        dataresponse.tps = tpscount;
        return countPenuh(req,res,dataresponse,query);
    });
}

function countSensor(req,res,dataresponse,query) {
    Sensor.count(query,function (err,sensorcount) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        dataresponse.sensor = sensorcount;
        return countTps(req,res,dataresponse,query);
    });
}

function countPetugas(req,res,dataresponse,query) {
    Petugas.count(query,function (err,petugascount) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        dataresponse.petugas = petugascount;
        return countSensor(req,res,dataresponse,query);
    })
}

function countAdmin(req,res,dataresponse,query) {
    Admin.count(query,function (err,admincount) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        dataresponse.admin = admincount;
        return countPetugas(req,res,dataresponse,query);
    });
}


exports.getData = function (req,res) {
    let dataresponse = {};
    let query= {};
    if(req.params.hasOwnProperty('wilayah')){
        query.wilayah = req.params.wilayah;
    }
    return countAdmin(req,res,dataresponse,query);
};