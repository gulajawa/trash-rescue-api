/**
 * Created by muharizals on 10/03/2017.
 */
const Pengangkutan  = require('../models/Pengangkutan'),
      Helper = require('../helper/ResponseHelper'),
      Util = require('../util/util'),
      Constant = require('../util/constants');


exports.getAll=function(req,res){
    let year = null;

    let query= {};
    if(req.params.hasOwnProperty('wilayah')){
        query.wilayah = req.params.wilayah;
    }

    if(Util.validateParameter(req,["year"])){
        year = parseInt(req.params.year);
    }

    if(year){
        let start =new Date(year,0,1);
        year++;
        let end = new Date(year,0,1);
        Pengangkutan
            .find(query)
            .where('createdAt').gte(start).lt(end)
            .sort('createdAt')
            .populate('tps','name address')
            .populate('petugas','name')
            .exec(function (err,angkut) {
                if(err){
                    return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                }
                return Helper.responseHelper(res,false,false,angkut);
            });
    }else{
        Pengangkutan
            .find(query)
            .sort('createdAt')
            .populate('tps','name address')
            .populate('petugas','name')
            .exec(function (err,angkut) {
                if(err){
                    return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                }
                return Helper.responseHelper(res,false,false,angkut);
            });
    }
};

exports.getSingle=function (req,res) {
    Pengangkutan.findOne({'_id':req.params.id})
        .populate('tps','name address')
        .populate('petugas','name')
        .exec(function (err,angkut) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }
            return Helper.responseHelper(res,false,false,angkut);
        });
};

exports.getByWhat=function (req,res) {
        let year = null;
        if(Util.validateParameter(req,["year"])){
            year = parseInt(req.params.year);
        }
      if(Util.validateParameter(req,["tps"])){
          if(year){
              let start =new Date(year,0,1);
              year++;
              let end = new Date(year,0,1);
              Pengangkutan
                  .find({})
                  .where('createdAt').gte(start).lt(end)
                  .where('tps').equals(req.params.tps)
                  .sort('createdAt')
                  .populate('tps','name address')
                  .populate('petugas','name')
                  .exec(function (err,angkut) {
                      if(err){
                          return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                      }
                      return Helper.responseHelper(res,false,false,angkut);
                  });
          }else{
              Pengangkutan
                  .find({})
                  .where('tps').equals(req.params.tps)
                  .sort('createdAt')
                  .populate('tps','name address')
                  .populate('petugas','name')
                  .exec(function (err,angkut) {
                      if(err){
                          return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                      }
                      return Helper.responseHelper(res,false,false,angkut);
                  });
          }
      }
      else if(Util.validateParameter(req,["petugas"])){
          if(year){
              let start =new Date(year,0,1);
              year++;
              let end = new Date(year,0,1);
              Pengangkutan
                  .find({})
                  .where('createdAt').gte(start).lt(end)
                  .where('petugas').equals(req.params.petugas)
                  .sort('createdAt')
                  .populate('tps','name address')
                  .populate('petugas','name')
                  .exec(function (err,angkut) {
                      if(err){
                          return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                      }
                      return Helper.responseHelper(res,false,false,angkut);
                  });
          }else{
              Pengangkutan
                  .find({})
                  .where('petugas').equals(req.params.petugas)
                  .sort('createdAt')
                  .populate('tps','name address')
                  .populate('petugas','name')
                  .exec(function (err,angkut) {
                      if(err){
                          return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                      }
                      return Helper.responseHelper(res,false,false,angkut);
                  });
          }
      }else{
          return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
      }

};

exports.getStatisticData=function (req,res) {
    if(Util.validateParameter(req,["tps"])){
        Pengangkutan.getStatistikByTps(req.params,function (err,result) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }
            return Helper.responseHelper(res,false,false,result);
        });
    }
    else if(Util.validateParameter(req,["petugas"])){
        Pengangkutan.getStatistikByPetugas(req.params,function (err,result) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }
            return Helper.responseHelper(res,false,false,result);
        })
    }
    else{
        Pengangkutan.getStatistik(req.params,function (err,result) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }
            return Helper.responseHelper(res,false,false,result);
        });
    }
};

exports.deleteSingle=function (req,res) {
    if(!Util.validateParameter(req,["id"])){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
    Pengangkutan.findOneAndRemove({'_id':req.params.id},function (err,angkut) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        else{
            if(Util.isEmpty(result)){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }else{
                return Helper.responseHelper(res,false,result);
            }
        }
    });
};

exports.deleteAll=function(req,res){
    let query= {};
    if(req.params.hasOwnProperty('wilayah')){
        query.wilayah = req.params.wilayah;
    }
    Pengangkutan.remove(query,function (err) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }else{
            return Helper.responseHelper(res,false);
        }
    });
};