/**
 * Created by muharizals on 10/02/2017.
 */

const Petugas = require('../models/Petugas'),
      Pengangkutan = require('../models/Pengangkutan'),
      Util = require('../util/util'),
     validator = require('email-validator'),
      Helper = require('../helper/ResponseHelper'),
      Constant = require('../util/constants'),
      passwordHash = require('password-hash'),
      EmailHelper = require('../helper/SendEmailHelper'),
      TelegramHelper = require('../helper/TelegramHelper');
const SMSHelper = require('../helper/SmsHelper');

exports.getPetugasToSendNotifApaSudahDiambil = function (tps,container) {
    Petugas.find({tps:tps.id},'name job id_telegram',function (err,result) {
        if(!err){
            if(!Util.isEmpty(result)){
                result.forEach(function (item) {
                        let isada=false;
                        for (let i=0;i<item.job.length;i++){
                            if(item.job[i].tps == tps.id){
                                isada = true;
                                break;
                            }
                        }
                        if(isada){
                            TelegramHelper.sendNotifIsKosong(tps,container,item);
                        }

                });
            }
        }
    });
};

exports.getPetugasToSendNotif = function (tps,container) {
        Petugas.find({tps:tps.id},'name job id_telegram no_hp',function (err,result) {
           if(!err){
               if(!Util.isEmpty(result)){
                   result.forEach(function (item) {
                      let pengangkutanbaru = new Pengangkutan({
                          petugas:item.id,
                          tps:tps.id,
                          wilayah:tps.wilayah
                      });
                      pengangkutanbaru.save();
                      let job = {
                          tps:tps.id,
                          container:container._id,
                          angkut:pengangkutanbaru._id
                      };
                      if(item.job.length <= 0){
                          Petugas.update({_id:item.id},{$addToSet:{job:job}},function (err) {
                              if(!err){
                                  if(item.id_telegram){
                                      TelegramHelper.sendNotifToPetugas(tps,container,item);
                                  }else{
                                      SMSHelper.sendSms(tps,item);
                                  }
                              }
                          });
                      }else{
                          let isada=false;
                          for (let i=0;i<item.job.length;i++){
                              if(item.job[i].tps == tps.id){
                                  isada = true;
                                  break;
                              }
                          }
                          if(!isada){
                              Petugas.update({_id:item.id},{$addToSet:{job:job}},function (err) {
                                  if(!err){
                                      if(item.id_telegram){
                                          TelegramHelper.sendNotifToPetugas(tps,container,item);
                                      }else{
                                          SMSHelper.sendSms(tps,item);
                                      }
                                  }
                              });
                          }
                      }

                   });
               }
           }
        });
};

exports.deleteJobPetugas = function (req,res) {
    if(!req.params.hasOwnProperty("id_petugas","id_job")){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED);
    }
    Petugas.findOne({_id:req.params.id_petugas},'job',function (err,result) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        if(Util.isEmpty(result)){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_USER);
        }
        result.job.pull(req.params.id_job);
        result.save();
        return Helper.responseHelper(res,false,false,result);
    });
};

exports.getSinglePetugas = function (req,res) {
    Petugas.findOne({_id:req.params.id},{__v:0,password:0,tmppassword:0}).
        populate('wilayah tps').
        exec(function (err,result) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }else{
                if(Util.isEmpty(result)){
                    return Helper.responseHelper(res,true,Constant.UNKNOWN_USER);
                }else{
                    return Helper.responseHelper(res,false,false,result);
                }
           }
        });
};


exports.getAllPetugas = function (req,res) {
    let query= {};
    if(req.params.hasOwnProperty('wilayah')){
        query.wilayah = req.params.wilayah;
    }
    Petugas.find(query,{__v:0,password:0,tmppassword:0}).populate('tps wilayah','-__v -container').exec(function (err,data) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }else{
            if(Util.validateParameter(req,["limit","offset"])){
                let offset = req.params.offset,limit = req.params.limit,selisih = offset+limit;
                return Helper.responseHelper(res,false,false,data.slice(offset,selisih));
            }else{
                return Helper.responseHelper(res,false,false,data);
            }
        }
    });
};

function updatePetugas(req,res) {
    if(req.params.hasOwnProperty("password")){
        if(Util.validateLength(req.params.password)){
            return Helper.responseHelper(res,true,Constant.PASSWORD_TOO_SHORT);
        }
        req.params.password = passwordHash.generate(req.params.password);
    }
    Petugas.updateById(req.params.id,req.params,function (err,result) {
        if(err){
            if(err.codeName == "DuplicateKey"){
                if(req.params.hasOwnProperty("no_hp")){
                    return Helper.responseHelper(res,true,Constant.PHONE_NUMBER_ALREADY_USE);
                }
            }
            return Helper.responseHelper(res,true,Constant.UNKNOWN_USER);
        }else{
            if(Util.isEmpty(result)){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_USER);
            }

            if(req.params.hasOwnProperty("email") && ( (req.params.email != result.email) || !result.email )){
                req.params.username = result.name;
                EmailHelper.sendUpdateEmail(req.params);

            }
            Petugas.findById(req.params.id)
                .populate('wilayah')
                .exec(function (err,user) {
                if(err){
                    return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                }
                else{
                    user = Util.deleteElement(user,["_id","password","__v","tmppassword"]);
                    return Helper.responseHelper(res,false,null,user);
                }
            });
        }
    });
}


exports.addTpsToPetugas = function (req,res) {
  if(!Util.validateParameter(req,["id","id_tps"])){
      return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
  }

  require('../models/TpsModel').findById(req.params.id_tps,function (e1,hasil) {
     if(e1 || Util.isEmpty(hasil)){
         return Helper.responseHelper(res,true,Constant.INVALID_TPS_ID);
     }

      Petugas.findById(req.params.id,"-password -__v",function (err,found) {
          if(err || Util.isEmpty(found)){
              return Helper.responseHelper(res,true,Constant.INVALID_PETUGAS_ID);
          }
          Petugas.update({_id:req.params.id},{$addToSet:{tps:req.params.id_tps}},function (err) {
              if(err){
                  return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
              }
              Petugas.findById(req.params.id,"-password -__v",function (err,temu) {
                 if(!err){
                     return Helper.responseHelper(res,false,false,temu);
                 }
              });
          });

      });
  });

};

exports.deleteSinglePetugasTps = function (req,res) {
    if(!Util.validateParameter(req,["id","id_tps"])){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
    Petugas.findById(req.params.id,function (err,result) {
        if(err || Util.isEmpty(result)){
            return Helper.responseHelper(res,true,Constant.INVALID_PETUGAS_ID,false,400);
        }
        if(result.tps.length > 0){
            result.tps.pull(req.params.id_tps);
            result.save(function (err) {
                if(err){
                    return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                }
                return Helper.responseHelper(res,false,false,result);
            });
        }else{
            return Helper.responseHelper(res,false,false,result);
        }
    });
};

exports.deleteTpsAllPetugas=function (req,res) {
    if(!Util.validateParameter(req,["id"])){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
    Petugas.findById(req.params.id,"-password -__v",function (err,result) {
       if(err){
           return Helper.responseHelper(res,true,Constant.INVALID_PETUGAS_ID);
       }
       result.tps = [];
       result.save(function (err) {
          if(err){
              return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
          }
          return Helper.responseHelper(res,false,false,result);
       });
    });
};

exports.updateSinglePetugas = function (req,res) {
    if(!Util.validateParameter(req,["id"])){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
    if(Util.validateParameter(req,["email"])){
        if(!validator.validate(req.params.email)){
            return Helper.responseHelper(res,true,Constant.INVALID_EMAIL_ADDRESS);
        }
        Petugas.findSimilarEmail(req.params.email,function (err,count) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }
            else{
                if(count==0){
                   return updatePetugas(req,res);
                }else{
                    return Helper.responseHelper(res,true,Constant.EMAIL_ALREADY_USE);
                }
            }
        });
    }else{
        return updatePetugas(req,res);
    }
};


function createNewAccount(req,res) {

    if(req.params.hasOwnProperty("password")){
        if(Util.validateLength(req.params.password)){
            return Helper.responseHelper(res,true,Constant.PASSWORD_TOO_SHORT);
        }
        req.params.password = passwordHash.generate(req.params.password);
    } 

    let newpetugas = new Petugas(req.params);
    newpetugas.save(function (err) {
        if(err){
            try{
                if(err.errors.hasOwnProperty('no_hp')){
                    return Helper.responseHelper(res,true,Constant.PHONE_NUMBER_ALREADY_USE);
                }
                if(err.errors.hasOwnProperty('email')){
                    return Helper.responseHelper(res,true,Constant.EMAIL_ALREADY_USE);
                }

            } catch (err2){
            }
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR,err);
        }
        else{
            if(req.params.hasOwnProperty("email")){
                EmailHelper.sendGreeting(newpetugas);
            }
            Petugas.findOne({_id:newpetugas.id}).
                populate('wilayah').
                exec(function (err2,petugas) {
                    if(err){
                        return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                    }
                    let option = Util.getOptionForToken(petugas);
                    petugas = Util.deleteElement(petugas,["password","__v"]);

                    petugas.token = Util.getUniqueToken(option);
                    return Helper.responseHelper(res,false,false,petugas,201);
                });
        }

    });
}

exports.createNewPetugas = function (req,res) {
    if(Util.validateParameter(req,["name","no_hp","wilayah"])){
        if(req.params.hasOwnProperty("email")){
            if(!validator.validate(req.params.email)){
                return Helper.responseHelper(res,true,Constant.INVALID_EMAIL_ADDRESS);
            }
            Petugas.findSimilarEmail(req.params.email,function (err,count) {
               if(err){
                   return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
               }
               else{
                   if(count==0){
                       return createNewAccount(req,res);
                   }else{
                       return Helper.responseHelper(res,true,Constant.EMAIL_ALREADY_USE);
                   }
               }
            });
        }else{
            return createNewAccount(req,res);
        }
    }else{
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
};

exports.deleteAllPetugas = function (req,res) {
    let query= {};
    if(req.params.hasOwnProperty('wilayah')){
        query.wilayah = req.params.wilayah;
    }
  Petugas.remove(query,function (err) {
      if(err){
          return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
      }else{
          return Helper.responseHelper(res,false);
      }
  });
};

exports.deletePetugas = function (req,res) {
    if(Util.validateParameter(req,["id"])){
        Petugas.findOneAndRemove({_id:req.params.id},function (err,result) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_USER);
            }
            else{
                if(Util.isEmpty(result)){
                    return Helper.responseHelper(res,true,Constant.UNKNOWN_USER);
                }else{
                    return Helper.responseHelper(res,false,result);
                }
            }
        });

    }else{
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
};