/**
 * Created by muharizals on 02/02/2017.
 */
 //mongoose = require('mongoose'),
const   Admin = require('../models/Admin'),
    Util = require('../util/util'),
    Helper = require('../helper/ResponseHelper'),
    validator = require('email-validator'),
    passwordHash = require('password-hash'),
    Constant = require('../util/constants'),
    EmailHelper = require('../helper/SendEmailHelper');

exports.getAllAdministrator = function (req,res) {
    Admin.find({},{__v:0,password:0})
        .populate('wilayah')
        .exec(function (err,data) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }else{
                if(Util.validateParameter(req,["limit","offset"])){
                    let offset = req.params.offset,limit = req.params.limit,selisih = offset+limit;
                    return Helper.responseHelper(res,false,false,data.slice(offset,selisih));
                }else{
                    return Helper.responseHelper(res,false,false,data);
                }
            }
        });
};

exports.createNewAdmin = function (req,res) {
    if(Util.validateParameter(req,["email","username","password","role"])){
        if(!validator.validate(req.params.email)){
            return Helper.responseHelper(res,true,Constant.INVALID_EMAIL_ADDRESS);
        }

        if(!Util.validateUsername(req.params.username)){
            return Helper.responseHelper(res,true,Constant.INVALID_USERNAME);
        }

        if(Util.validateLength(req.params.username)){
            return Helper.responseHelper(res,true,Constant.USERNAME_TOO_SHORT);
        }

        if(Util.validateLength(req.params.password)){
            return Helper.responseHelper(res,true,Constant.PASSWORD_TOO_SHORT);
        }

        if(Util.validateRole(req.params.role)){
            return Helper.responseHelper(res,true,Constant.INVALID_ROLE);
        }

        req.params.role = req.params.role.toUpperCase();

        if(req.params.role == "ADMIN"){
            if(!req.params.hasOwnProperty('wilayah')){
                return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
            }
        }

        req.params.password = passwordHash.generate(req.params.password);

        let newadmin = new Admin(req.params);
        newadmin.save(function (err) {
           if(err){
                try{
                    if(err.errors.hasOwnProperty('email')){
                        return Helper.responseHelper(res,true,Constant.EMAIL_ALREADY_USE);
                    }
                    if(err.errors.hasOwnProperty('username')){
                        return Helper.responseHelper(res,true,Constant.USERNAME_ALREADY_USE);
                    }
                } catch (err2){

                }
               return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
           }else{
               EmailHelper.sendGreeting(newadmin);
               let option = Util.getOptionForToken(newadmin);
               newadmin = Util.deleteElement(newadmin,["password","__v"]);

               newadmin.token = Util.getUniqueToken(option);
               return Helper.responseHelper(res,false,false,newadmin,201);
           }

        });
    }else{
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
};

function validateUser(req,res,user) {
    if(Util.isEmpty(user)){
        return Helper.responseHelper(res,true,Constant.UNKNOWN_USER);
    }
    if(passwordHash.verify(req.params.password,user.password) || (user.tmppassword == req.params.password)){
        if(user.tmppassword){
            user.tmppassword = undefined;
            user.save(function (err) {});
        }
        let option = Util.getOptionForToken(user);
        user = Util.deleteElement(user,["_id","password","__v","tmppassword"]);
        user.token = Util.getUniqueToken(option);
        return Helper.responseHelper(res,false,null,user);
    }

    return Helper.responseHelper(res,true,Constant.INVALID_PASSWORD);
}

exports.adminAuth=function (req,res) {
    if(Util.validateParameter(req,["username","password"])){
        Admin.find().byUsername(req.params.username)
            .populate('wilayah')
            .exec(function (err,admin) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }else{
                return validateUser(req,res,admin);
            }
        });
    }
    else if(Util.validateParameter(req,["email","password"])){
        if(!validator.validate(req.params.email)){
            return Helper.responseHelper(res,true,Constant.INVALID_EMAIL_ADDRESS);
        }
        Admin.find().byEmail(req.params.email).exec(function (err,admin) {
           if(err){
               return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
           }else{
               return validateUser(req,res,admin);
           }
        });

    }
    else{
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }

};

exports.updateAdmin=function (req,res){
    if(Util.validateParameter(req,["email"])){
        if(!validator.validate(req.params.email)){
            return Helper.responseHelper(res,true,Constant.INVALID_EMAIL_ADDRESS);
        }
    }
    if(Util.validateParameter(req,["username"])){
        if(!Util.validateUsername(req.params.username)){
            return Helper.responseHelper(res,true,Constant.INVALID_USERNAME);
        }

        if(Util.validateLength(req.params.username)){
            return Helper.responseHelper(res,true,Constant.USERNAME_TOO_SHORT);
        }
    }
    if(Util.validateParameter(req,["password"])){
        if(Util.validateLength(req.params.password)){
            return Helper.responseHelper(res,true,Constant.PASSWORD_TOO_SHORT);
        }
        req.params.password = passwordHash.generate(req.params.password);
    }

    if(req.params.hasOwnProperty('role')){
        if(Util.validateRole(req.params.role)){
            return Helper.responseHelper(res,true,Constant.INVALID_ROLE);
        }
        req.params.role = req.params.role.toUpperCase();
        if(req.params.role == "ADMIN"){
            if(!req.params.hasOwnProperty('wilayah')){
                return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
            }
        }
    }

    if(!Util.isEmpty(req.params)){
        let query = req.params.hasOwnProperty('id') ? req.params.id:req.user.id;
        Admin.updateById(query,req.params,function (err,result) {
            if(err){
                if(err.codeName == "DuplicateKey"){
                    if(req.params.hasOwnProperty("email")){
                        return Helper.responseHelper(res,true,Constant.EMAIL_ALREADY_USE);
                    }
                    if(req.params.hasOwnProperty("username")){
                        return Helper.responseHelper(res,true,Constant.USERNAME_ALREADY_USE);
                    }
                }

                return Helper.responseHelper(res,true,Constant.UNKNOWN_USER);
            }
            else{
                if(req.params.hasOwnProperty("email") && (req.params.email != result.email)){
                    EmailHelper.sendUpdateEmail(req.params.email);
                }
                Admin.findById(req.user.id).exec(function (err,user) {
                    if(err){
                        return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                    }else{
                        user = Util.deleteElement(user,["_id","password","__v","tmppassword"]);
                        return Helper.responseHelper(res,false,null,user);
                    }
                });
            }
        });
    }else{
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }


};


function saveTempPassword(res,user) {
    user.tmppassword= Util.getRandomPassword();
    user.save(function (err) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        else{
            EmailHelper.sendForgetPassword(user);
           return Helper.responseHelper(res,false);
        }
    });
};

exports.forgetPassword = function (req,res) {
    if(req.params.hasOwnProperty("email")){
        if(!validator.validate(req.params.email)){
            return Helper.responseHelper(res,true,Constant.INVALID_EMAIL_ADDRESS);
        }
        Admin.find().byEmail(req.params.email).exec(function (err,user) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }else{
                if(Util.isEmpty(user)) {
                    return Helper.responseHelper(res, true, Constant.UNKNOWN_EMAIL);
                }
                return saveTempPassword(res,user);
            }
        });
    }
    else if(req.params.hasOwnProperty("username")){
        Admin.find().byUsername(req.params.username).exec(function (err,user) {
           if(err){
               return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
           }else{
               if(Util.isEmpty(user)){
                   return Helper.responseHelper(res,true,Constant.UNKNOWN_USERNAME);
               }
               return saveTempPassword(res,user);
           }
        });
    }
    else{
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }

};

exports.deleteSingleAdmin=function (req,res) {
    if(!Util.validateParameter(req,["id"])){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
    Admin.findOneAndRemove({_id:req.params.id},function (err,result) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_USER);
        }
        else{
            if(Util.isEmpty(result)){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_USER);
            }else{
                return Helper.responseHelper(res,false,result);
            }
        }
    });
};

exports.deleteAccount=function (req,res) {
    Admin.remove().byId(req.user.id).exec(function (err) {
       if(err){
           return Helper.responseHelper(res,true,Constant.UNKNOWN_USER);
       }
       else{
           return Helper.responseHelper(res,false);
       }
    });
};