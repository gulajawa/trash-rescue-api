/**
 * Created by muharizals on 10/03/2017.
 */
const TelegramBot = require('node-telegram-bot-api'),
      Config = require('../../config.json'),
      telegram = new TelegramBot(Config.telegram.uid,{polling:false}),
      emoji = require('node-emoji');

exports.sendNotifToPetugas = function (tps,container,item) {
        if(item.id_telegram){
            let msg = emoji.emojify("Hai " + item.name + ":blush: :blush:");
            msg += "\n\nKontainer di tps <b>" + tps.name + "</b> sudah <b>PENUH</b>";
            msg += emoji.emojify("\n\nSilahkan di proses. \nTerimakasih :blush: :blush:");
            telegram.sendMessage(item.id_telegram,msg,{parse_mode:"html"});
            telegram.sendLocation(item.id_telegram,tps.location[1],tps.location[0]);
        }
};

exports.sendNotifIsKosong = function (tps,container,item) {
        if(item.id_telegram){
            let msg = "Hai " + item.name;
            msg += "\nApakah kontainer di tps <b>" + tps.name + "</b> sudah di ambil ??";
            telegram.sendMessage(item.id_telegram,msg,{parse_mode:"html"});
        }
};