/**
 * Created by muharizals on 13/04/2017.
 */

var io = null;

exports.setIo = function (soio) {
    io = soio;
};


exports.notiftySensorUpdate = function (data) {
  io.emit("sensor",data);
};

exports.notifyTPSUpdate = function (data) {
  io.emit("tps",data);
};

exports.notifyPengankutan = function (data) {
  io.emit("pengangkutan",data);
};