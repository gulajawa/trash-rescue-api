/**
 * Created by muharizals on 09/02/2017.
 */

const NodeGeoCoder = require('node-geocoder');

var option = {
    provider: 'google'
};

exports.getGeocoder=function(){
    return NodeGeoCoder(option);
};