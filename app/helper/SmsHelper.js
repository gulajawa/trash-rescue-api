/**
 * Created by muharizals on 06/06/2017.
 */
const  config = require('../../config.json');
const  Nexmo = require('nexmo');

function NexmoHelper() {
    this.sendSms = function (tps,item) {
        let message = "Hai " + item.name + "\n";
        message += "Kontainer di tps " + tps.name + " sudah PENUH.";
        message += "\nSilahkan diproses,terimakasih.";
        let nexmo = new Nexmo({
            apiKey: config.nexmo_key,
            apiSecret: config.nexmo_secret
        },{debug:false});
        nexmo.message.sendSms("TrashRescue", item.no_hp,message, function (err,data) {

        });
    };

    this.sendCustomSms = function (from,phone,message) {
        let nexmo = new Nexmo({
            apiKey: config.nexmo.apiKey,
            apiSecret: config.nexmo.apiSecret,
        },{debug:false});
        nexmo.message.sendSms(from, phone, message, function (err,data) {

        });
    };
}

module.exports = new NexmoHelper();