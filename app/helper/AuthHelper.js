/**
 * Created by muharizals on 07/02/2017.
 */

const  Admin = require('../models/Admin'),
       Helper = require('../helper/ResponseHelper'),
       Constant = require('../util/constants'),
       Sensor = require('../models/Sensor');

exports.isAdministrator=function (req,res,next) {
    Admin.count({_id: req.user.id}, function (err, count){
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }else{
            if(count>0){
                return next();
            }else{
                return Helper.responseHelper(res,true,Constant.PERMISSION_DENIED,false,401);
            }
        }
    });
};

exports.isSuperAdministrator=function (req,res,next) {
  Admin.count({_id:req.user.id,role:'SUPERADMIN'},function (err,count) {
      if(err){
          return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
      }else{
          if(count>0){
              return next();
          }else{
              return Helper.responseHelper(res,true,Constant.PERMISSION_DENIED,false,401);
          }
      }
  });
};

exports.validateIdSensor =function (req,res,next) {
    if(!req.params.hasOwnProperty('sensor_id')){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
    Sensor.count({_id:req.params.sensor_id},function (err,count) {
       if(err){
           return Helper.responseHelper(res,true,Constant.INVALID_SENSOR_ID);
       }else{
           if(count>0){
               return next();
           }else{
               return Helper.responseHelper(res,true,Constant.INVALID_SENSOR_ID);
           }
       }
    });
};

exports.validateApiKeySensor = function (req,res,next) {
    if(!req.params.hasOwnProperty('api_key') ){
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false,400);
    }
    Sensor.count({api_key:req.params.api_key},function (err,count) {
       if(err){
           return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
       }else{
           if(count>0){
               return next();
           }else{
               return Helper.responseHelper(res,true,Constant.INVALID_API_KEY);
           }
       }
    });
};
