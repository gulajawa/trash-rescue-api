/**
 * Created by muharizals on 04/02/2017.
 */

const nodemailler = require('nodemailer'),
      config = require('../../config'),
      validator = require('email-validator'),
      Util = require('../util/util'),
      Handlebars = require('handlebars');

var transporter = nodemailler.createTransport({
    service:'gmail',
    auth: {
        user: config.email.user,
        pass: config.email.pass
    }
});

exports.sendGreeting=function (person) {
    if(!validator.validate(person.email)){
        return;
    }

    let mailOptions = {
        from: ' " '+ config.email.from + '" <no-reply@arizalsaputro.net>',
        to: person.email,
        subject: 'Selamat datang',
        text: 'Selamat datang di trash rescue' +(person.name || person.username)
    };
    let path = 'app/templates/hello.html';
    Util.readHTMLFile(path,function (err,html) {
        if(!err){
            let template = Handlebars.compile(html);
            mailOptions.html  = template(person);

            transporter.sendMail(mailOptions,function (error,info) {});
        }
    });
};


exports.sendUpdateEmail=function (person) {
    if(!validator.validate(person.email)){
        return;
    }
    let mailOptions = {
        from: ' " '+ config.email.from + '" <no-reply@arizalsaputro.net>',
        to: person.email,
        subject: 'Pembaruan alamat email',
        text: 'Alamat email anda baru saja diperbaruhui' +(person.name || person.username)
    };
    let path = 'app/templates/update_email_address.html';
    Util.readHTMLFile(path,function (err,html) {
        if(!err){
            let templateChild = Handlebars.compile(html);
            mailOptions.html = templateChild(person);
            transporter.sendMail(mailOptions,function (err,info) {
                console.log(info);
            });
        }
    });
};

exports.sendForgetPassword=function (person) {
    if(!validator.validate(person.email)){
        return;
    }
    let mailOptions = {
        from: ' " '+ config.email.from + '" <no-reply@arizalsaputro.net>',
        to: person.email,
        subject: 'Lupa kata sandi',
        text: 'Sepertinya anda lupa kata sandi anda ' +(person.name || person.username)
    };
    let path = 'app/templates/forget_password.html';
    Util.readHTMLFile(path,function (err,html) {
        if(!err){
            let template = Handlebars.compile(html);
            mailOptions.html = template(person);
            transporter.sendMail(mailOptions,function (err,info) {});
        }
    });

};