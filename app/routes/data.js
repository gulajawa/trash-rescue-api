/**
 * Created by muharizals on 18/04/2017.
 */
const  dasboardController = require('../controllers/DashboardController'),
    AuthHelper = require('../helper/AuthHelper');

module.exports = function (app) {
    app.get('dashboard',AuthHelper.isAdministrator,dasboardController.getData);
};