/**
 * Created by muharizals on 20/02/2017.
 */

const tpsController = require('../controllers/TPSController'),
      AuthHelper = require('../helper/AuthHelper');

module.exports = function (app) {

    app.get('tps',AuthHelper.isAdministrator,tpsController.getListTps);
    app.get('tps/:id',AuthHelper.isAdministrator,tpsController.getSingleTPS);

    app.post('tps/new',AuthHelper.isAdministrator,tpsController.createNewTPS);
    app.post('tps/add/container',AuthHelper.isAdministrator,tpsController.addContainer);

    app.put('tps/update/container',AuthHelper.isAdministrator,tpsController.updateTinggiSingleContainer);
    app.put('tps/update',AuthHelper.isAdministrator,tpsController.updateSingleTPS);

    app.del('tps/remove/container',AuthHelper.isAdministrator,tpsController.removeSingleContainer);
    app.del('tps/remove/container/all',AuthHelper.isAdministrator,tpsController.removeAllContainer);
    app.del('tps/remove',AuthHelper.isAdministrator,tpsController.deleteSingleTps);
    app.del('tps/remove/all',AuthHelper.isAdministrator,tpsController.deleteAllTPS);


};