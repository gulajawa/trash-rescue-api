/**
 * Created by muharizals on 10/02/2017.
 */

const petugasController = require('../controllers/PetugasController'),
    AuthHelper = require('../helper/AuthHelper');

module.exports = function (app) {
    //get

    app.get('petugas',AuthHelper.isAdministrator,petugasController.getAllPetugas);
    app.get('petugas/:id',AuthHelper.isAdministrator,petugasController.getSinglePetugas);

    //new
    app.post('petugas/new',AuthHelper.isAdministrator,petugasController.createNewPetugas);

    //add tps
    app.post('petugas/add/tps',AuthHelper.isAdministrator,petugasController.addTpsToPetugas);

    //delete
    app.del('petugas/single/:id',AuthHelper.isAdministrator,petugasController.deletePetugas);
    app.del('petugas',AuthHelper.isAdministrator,petugasController.deletePetugas);
    app.del('petugas/all',AuthHelper.isAdministrator,petugasController.deleteAllPetugas);

    app.del('petugas/tps/all',AuthHelper.isAdministrator,petugasController.deleteTpsAllPetugas);
    app.del('petugas/tps',AuthHelper.isAdministrator,petugasController.deleteSinglePetugasTps);

    app.del('petugas/job/delete',AuthHelper.isAdministrator,petugasController.deleteJobPetugas);

    //update
    app.put('petugas/update/:id',AuthHelper.isAdministrator,petugasController.updateSinglePetugas);

};
