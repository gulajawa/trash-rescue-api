/**
 * Created by muharizals on 10/03/2017.
 */

const angkutController = require('../controllers/PengangkutanController'),
    AuthHelper = require('../helper/AuthHelper');

module.exports = function (app) {

    app.get('angkut',AuthHelper.isAdministrator,angkutController.getAll);
    app.get('angkut/:id',AuthHelper.isAdministrator,angkutController.getSingle);
    app.get('angkut/get/by',AuthHelper.isAdministrator,angkutController.getByWhat);
    app.get('angkut/get/statistik',AuthHelper.isAdministrator,angkutController.getStatisticData);

    app.del('angkut/remove',AuthHelper.isAdministrator,angkutController.deleteSingle);
    app.del('angkut/remove/all',AuthHelper.isAdministrator,angkutController.deleteAll);


};