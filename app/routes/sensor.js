/**
 * Created by muharizals on 08/02/2017.
 */

const sensorController = require('../controllers/SensorController'),
      AuthHelper = require('../helper/AuthHelper');

module.exports = function (app,io) {
    sensorController.setIo(io);
    //get
    app.get('sensor',AuthHelper.isAdministrator,sensorController.getSensor);
    app.get('sensor/:sensor_id',AuthHelper.isAdministrator,AuthHelper.validateIdSensor,sensorController.getSingleSensor);
    //new
    app.post('sensor/new',AuthHelper.isAdministrator,sensorController.createNewSensorV2);

    //update
    app.put('sensor/update',AuthHelper.validateApiKeySensor,sensorController.updateSingleSensor);
    app.get('sensor/single/update',AuthHelper.validateApiKeySensor,sensorController.updateSingleSensor);

    //delete
    app.del('sensor',AuthHelper.isAdministrator,AuthHelper.validateApiKeySensor,sensorController.deleteSingleSensor);
    app.del('sensor/all',AuthHelper.isAdministrator,sensorController.deleteAllSensor);

};

