/**
 * Created by muharizals on 02/02/2017.
 */

const jwt = require('restify-jwt'),
      config = require('../../config.json');

module.exports =function (app,io) {


    app.use(jwt({secret:config.superSecret}).unless({path: ['/','/admin/auth','/admin/forget','/sensor/update','/sensor/single/update','/admin/super/secret/create/super/admin/with/admin']}));
    //app.use(jwt({secret:config.superSecret}).unless({path: ['admin/new','/','/admin/auth','/admin/forget','/sensor/update','/sensor/single/update']}));

    app.get('/',function (req,res) {
     return res.send({
       "error": false,
       "message": "Trash Rescue Api by GULA JAWA Team"
     });
  });
    //dasboard

    require('./data')(app);

  //admin
    require('./admin')(app);

  //sensor
    require('./sensor')(app,io);

   //petugas
    require('./petugas')(app);

    //tps
    require('./tps')(app);

    //angkut
    require('./angkut')(app);

    //wilayah
    require('./wilayah')(app);
};