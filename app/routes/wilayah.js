/**
 * Created by muharizals on 12/05/2017.
 */
const wilayahController = require('../controllers/WilayahController'),
    AuthHelper = require('../helper/AuthHelper');

module.exports = function (app) {
    app.get('wilayah',AuthHelper.isAdministrator,wilayahController.getAll);
    app.post('wilayah',AuthHelper.isAdministrator,wilayahController.createNewWilayah);
    app.del('wilayah/:id',AuthHelper.isAdministrator,wilayahController.deleteWilayah);
    app.put('wilayah/:id',AuthHelper.isAdministrator,wilayahController.updateWilayah);
    app.del('wilayah/delete/all',AuthHelper.isAdministrator,wilayahController.deleteAllWilayah);
};