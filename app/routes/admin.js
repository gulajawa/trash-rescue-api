/**
 * Created by muharizals on 08/02/2017.
 */
const adminController = require('../controllers/AdminController'),
      AuthHelper = require('../helper/AuthHelper');

module.exports = function (app) {

    app.get('admin',AuthHelper.isAdministrator,adminController.getAllAdministrator); //get
    app.post('admin/new',AuthHelper.isAdministrator,AuthHelper.isSuperAdministrator,adminController.createNewAdmin);//new
    //app.post('admin/new',adminController.createNewAdmin);//new
    app.post('admin/auth',adminController.adminAuth); //auth
    app.post('admin/super/secret/create/super/admin/with/admin',adminController.createNewAdmin);
    app.put('admin/update',AuthHelper.isAdministrator,adminController.updateAdmin); //update
    app.post('admin/forget',adminController.forgetPassword); //forget
    app.del('admin/delete',AuthHelper.isAdministrator,AuthHelper.isSuperAdministrator,adminController.deleteAccount); //delete
    app.del('admin/delete/single',AuthHelper.isAdministrator,adminController.deleteSingleAdmin);//delete single
};




