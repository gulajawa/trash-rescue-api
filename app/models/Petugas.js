/**
 * Created by muharizals on 07/02/2017.
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    uniqueValidator = require('mongoose-unique-validator');

const pegawaiSchema = new Schema({
   name : {type:String,required:true},
   no_hp : {type:String,unique:true,required:true},
   id_telegram: String,
   email :String,
   password: String,
   tmppassword: String,
   tps:[{type:Schema.Types.ObjectId,ref:'TPS',unique:true}],
   job: [{tps:{type:Schema.Types.ObjectId,ref:'TPS'}, container: {type:Schema.Types.ObjectId},angkut:{type:Schema.Types.ObjectId}}],
   wilayah:{type:Schema.Types.ObjectId,ref:'Wilayah'}
},{timestamps:Date});



pegawaiSchema.statics.findSimilarEmail = function (email,cb) {
  return this.count({email:email},cb);
};

pegawaiSchema.statics.updateById = function (id,data,cb) {
   return this.findOneAndUpdate({_id:id},data,cb);
};

pegawaiSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Petugas',pegawaiSchema);
