/**
 * Created by muharizals on 02/02/2017.
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    uniqueValidator = require('mongoose-unique-validator');

const adminSchema = new Schema({
    name:{type:String},
    email:{type:String,unique:true,required:true},
    username:{type:String,unique:true,required:true},
    password:{type:String,unique:false,required:true},
    id_telegram:{type:String},
    tmppassword:{type:String},
    role: {type:String,enum:["SUPERADMIN","ADMIN"]},
    wilayah:{type:Schema.Types.ObjectId,ref:'Wilayah'}
},{timestamps:Date});

adminSchema.query.byUsername = function (username) {
    return this.findOne({username:username});
};

adminSchema.query.byEmail = function (email) {
    return this.findOne({email:email});
};

adminSchema.query.byId = function (id) {
   return this.remove({_id:id});
};

adminSchema.statics.updateById=function (id,data,cb) {
  return this.findOneAndUpdate({_id:id},data,cb);
};



adminSchema.plugin(uniqueValidator);


module.exports = mongoose.model('Admin',adminSchema);