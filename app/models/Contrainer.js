/**
 * Created by muharizals on 20/02/2017.
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const containerSchema = new Schema({
    tinggi_awal: Number,
    status: {type:String,enum:["PENUH","KOSONG","TIDAK_DIKETAHUI","SETENGAH_PENUH","HAMPIR_PENUH"]},
    jam_penuh: Date,
    sensor:{type:Schema.Types.ObjectId,ref:'Sensor'},
    ukuran:String
});

module.exports = containerSchema;
