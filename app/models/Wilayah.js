/**
 * Created by muharizals on 12/05/2017.
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const wilayahSchema = new Schema({
    name:String
},{timestamps:Date});

module.exports = mongoose.model("Wilayah",wilayahSchema);