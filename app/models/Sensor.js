/**
 * Created by muharizals on 08/02/2017.
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    config = require('../../config');

const sensorSchema = new Schema({
    api_key:{type:String,unique:true},
    jarak_sensor_1:Number,
    jarak_sensor_2:Number,
    location:{
        type:[Number],
        index:'2d'
    },
    wilayah:{type:Schema.Types.ObjectId,ref:'Wilayah'}
},{timestamps:Date});

sensorSchema.pre('remove',function (next) {
    this.model('TPS').update({'container.sensor':this._id},{'$set':{
        'container.$.sensor' : undefined,'container.$.status':'TIDAK_DIKETAHUI'
    }},next);
});

sensorSchema.statics.countActiveSensor = function (params,cb) {
    var time = new Date().getTime() - config.intervalSensor*60*1000 ;
    let match = {
        $match:{
            updatedAt : {$gt:new Date(time)}
        }
    };
    if(params.wilayah){
        match.$match.wilayah = mongoose.Types.ObjectId(params.wilayah);
    }
    return this.aggregate([
       match,
        {
            $group: {
                _id: null,
                count: {$sum: 1}
            }
        }
    ],cb);
};

module.exports = mongoose.model("Sensor",sensorSchema);