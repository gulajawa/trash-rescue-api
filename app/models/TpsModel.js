/**
 * Created by muharizals on 08/02/2017.
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Container = require('./Contrainer');

const tpsSchema = new Schema({
    name : {type:String,required:true},
    location:{
        type:[Number],
        index:'2d'
    },
    address:String,
    kecamatan:String,
    container:[Container],
    wilayah:{type:Schema.Types.ObjectId,ref:'Wilayah'}
},{timestamps:Date});

tpsSchema.statics.updateTinggiContainer = function (id,container_id,tinggi_awal) {
    return this.update({'_id':id,'container._id':container_id},{'$set':{
        'container.$.tinggi_awal' : tinggi_awal
    }});
};

tpsSchema.statics.removeContainerSensor = function (id_sensor) {
    return this.update({'container.sensor':id_sensor},{'$set':{
        'container.$.sensor' : undefined,'container.$.status':'TIDAK_DIKETAHUI','jam_penuh':undefined
    }});
};

tpsSchema.statics.updateTpsContainer = function (id,container_id,status,jam_penuh,sensor) {
    return this.update(
        {'_id':id,'container._id':container_id},
        {'$set':
            {'container.$.status':status,'container.$.jam_penuh':jam_penuh,'container.$.sensor':sensor}
        });
};

tpsSchema.pre('remove',function (next) {
    this.model('Petugas').update({},{
        '$pull':{
            'tps':this._id
        }
    },{safe:true,multi:true},next);
});

module.exports = mongoose.model("TPS",tpsSchema);