/**
 * Created by muharizals on 10/03/2017.
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const angkutSchema = new Schema({
    petugas:{type:Schema.Types.ObjectId,ref:'Petugas'},
    tps:{type:Schema.Types.ObjectId,ref:'TPS'},
    wilayah:{type:Schema.Types.ObjectId,ref:'Wilayah'}
},{timestamps:Date});

angkutSchema.statics.getStatistikByPetugas=function (params,cb) {
    let match = {
        $match:{
            petugas:  mongoose.Types.ObjectId(params.petugas)
        }
    };
    if(params.hasOwnProperty('wilayah')){
        match.$match.wilayah = mongoose.Types.ObjectId(params.wilayah);
    }
    return this.aggregate([
        match,
        {
            $project:{
                y:{
                    $year:"$createdAt"
                },
                m:{
                    $month:"$createdAt"
                },
                d:{
                    $dayOfMonth: "$createdAt"
                }

            }
        },
        {
            $group:{
                _id:{year:"$y",month:"$m",day:"$d"},
                count:{$sum:1}
            }
        },
        {
            $sort:{"_id":1}
        },
        {
            $group:{
                _id:{year:"$_id.year",month:"$_id.month"},
                dailyusage:{$push:{day:"$_id.day",count:"$count"}},
                count:{$sum:"$count"}
            }
        },
        {
            $sort:{"_id":1}
        },
        {
            $group:{
                _id:"$_id.year",
                monthlyusage:{$push:{month:"$_id.month",dailyusage:"$dailyusage",count:"$count"}},
                count:{$sum:"$count"}
            }
        }
    ],cb);
};


angkutSchema.statics.getStatistikByTps=function (params,cb) {
    let match = {
        $match:{
            tps:  mongoose.Types.ObjectId(params.tps)
        }
    };
    if(params.hasOwnProperty('wilayah')){
        match.$match.wilayah = mongoose.Types.ObjectId(params.wilayah);
    }
    return this.aggregate([
      match,
      {
          $project:{
              y:{
                  $year:"$createdAt"
              },
              m:{
                  $month:"$createdAt"
              },
              d:{
                  $dayOfMonth: "$createdAt"
              }

          }
      },
      {
          $group:{
              _id:{year:"$y",month:"$m",day:"$d"},
              count:{$sum:1}
          }
      },
      {
          $sort:{"_id":1}
      },
      {
          $group:{
              _id:{year:"$_id.year",month:"$_id.month"},
              dailyusage:{$push:{day:"$_id.day",count:"$count"}},
              count:{$sum:"$count"}
          }
      },
      {
          $sort:{"_id":1}
      },
      {
          $group:{
              _id:"$_id.year",
              monthlyusage:{$push:{month:"$_id.month",dailyusage:"$dailyusage",count:"$count"}},
              count:{$sum:"$count"}
          }
      }
  ],cb);
};

angkutSchema.statics.getStatistik= function (params,cb) {
    let match = {
        $match:{

        }
    };
    if(params.hasOwnProperty('wilayah')){
        match.$match.wilayah = mongoose.Types.ObjectId(params.wilayah);
    }
    return this.aggregate([
        match,
        {
           $project:{
               y:{
                   $year:"$createdAt"
               },
               m:{
                   $month:"$createdAt"
               },
               d:{
                   $dayOfMonth: "$createdAt"
               }

           }
        },{
            $group:{
                _id:{year:"$y",month:"$m",day:"$d"},
                count:{$sum:1}
            }
        },
        {
            $sort:{"_id":1}
        },
        {
            $group:{
                _id:{year:"$_id.year",month:"$_id.month"},
                dailyusage:{$push:{day:"$_id.day",count:"$count"}},
                count:{$sum:"$count"}
            }
        },
        {
            $sort:{"_id":1}
        },
        {
            $group:{
                _id:"$_id.year",
                monthlyusage:{$push:{month:"$_id.month",dailyusage:"$dailyusage",count:"$count"}},
                count:{$sum:"$count"}
            }
        }
    ],cb);
};



module.exports = mongoose.model("Pengangkutan",angkutSchema);

