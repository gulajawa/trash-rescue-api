/**
 * Created by muharizals on 02/02/2017.
 */

const util = require('util'),
    fs = require('fs'),
    jwtoken = require('jsonwebtoken'),
    config = require('../../config.json'),
    randomstring = require('randomstring');



exports.getRandomString=function(length,charset){
    let option ={};
    if(length){
        option.length = length;
    }
    if(charset){
        option.charset = charset;
    }
    return randomstring.generate(option);
};

exports.getInitContainer=function () {
    let container = {
        tinggi_awal : config.XDistance,
        status:"TIDAK_DIKETAHUI",
        ukuran:"6 m3"
    };
    return container;
};

exports.getRandomPassword=function () {
    return randomstring.generate({length:12});
};

exports.getUniqueApiKey=function (length) {
  return randomstring.generate({length:length});
};



exports.getUniqueToken = function (user) {
    return jwtoken.sign(user,config.superSecret,{ expiresIn: config.tokenExpiresIn });
};

exports.readHTMLFile = function(path, callback) {
    fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
        if (err) {
            throw err;
            callback(err);
        }
        else {
            callback(null, html);
        }
    });
};

exports.deleteElementV2=function(data,args){
    args.forEach(function (arg) {
        delete data[arg];
    });
    return data;
};

exports.deleteElement=function (data,args) {
   data = data.toObject();
   args.forEach(function (arg) {
        delete data[arg];
   });
   return data;
};

exports.validateParameter=function (req,args) {
     let status = true;
     for (let i=0;i<args.length;i++) {
         status = req.params.hasOwnProperty(args[i]);
         if (!status) {
             break;
         }
     }
    return status;
};

exports.isEmpty=function(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
};

exports.getOptionForToken=function (data) {
    let options = {};
    options.id = data.id;
    return options;
};

exports.validateUsername=function(req){
  return util.isString(req);
};

exports.cekNumber = function (req) {
    return util.isNumber(req);
};

exports.validateLength=function(req){
    req = req.replace(/\s+/g, '');
   return req.length < 6;
};

exports.validateRole=function (role) {
  role = role.toUpperCase();
  if(role == "ADMIN" || role == "SUPERADMIN"){
      return false;
  }
  return true;
};