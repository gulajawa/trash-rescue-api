/**
 * Created by muharizals on 28/02/2017.
 */

const TelegramBot = require('node-telegram-bot-api'),
    ContextHelper = require('./telegram/helper/ContextHelper'),
    CommandHelper = require('./telegram/helper/CommandHelper'),
    BaseController = require('./telegram/controller/BaseController'),
    Config = require('../../config.json');
    telegram = new TelegramBot(Config.telegram.uid,{polling:true});

console.log('Trash rescure bot ready');



telegram.on('message',function (msg) {

    if(CommandHelper.isCommands(msg.text)){
        CommandHelper.processCommand(msg,telegram,ContextHelper);
        return;
    }
    if(ContextHelper.getConversationContext(msg.from.id) != null){
        BaseController.processContext(msg,telegram,ContextHelper);
        return;
    }

    BaseController.processPegawaiMessage(msg,telegram,ContextHelper);

});

telegram.on('callback_query', function (msg) {
    BaseController.processInlineQuery(msg,telegram,ContextHelper);
});