/**
 * Created by muharizals on 01/03/2017.
 */

const  Constant = require('../util/Constants'),
       AdminController = require('./AdminController'),
       PetugasController = require('./PetugasController');

exports.processContext = function (msg,bot,ContextHelper) {
    let concon = ContextHelper.getConversationContext(msg.from.id);
    switch (concon.context){
        case Constant.con_force_admin:
            AdminController.con_force_admin(msg,bot,ContextHelper,concon.subcontext);
            break;
        case Constant.con_admin:
            AdminController.con_admin(msg,bot,ContextHelper,concon.subcontext);
            break;

    }
};

exports.processInlineQuery=function (msg,bot,ContextHelper) {
    let str = msg.data;
    if(str.includes("admin") || str.includes("idtelegramadmin") || str.includes("idtelegrampegawai")){
      AdminController.adminInlineQuery(msg,bot,ContextHelper);
    }
    if(str.includes("ptgspil")){
        PetugasController.petugasInlineQuery(msg,bot,ContextHelper);
    }

};

exports.processPegawaiMessage=function (msg,bot,ContextHelper) {
    PetugasController.cekPetugasPengangkutan(msg,bot,ContextHelper);
};