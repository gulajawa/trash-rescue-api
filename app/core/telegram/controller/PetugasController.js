/**
 * Created by muharizals on 02/03/2017.
 */

const Petugas = require('../../../models/Petugas'),
      TextHelper = require('../helper/TextHelper'),
      Constant = require('../util/Constants'),
      emoji = require('node-emoji'),
      TPS = require('../../../models/TpsModel'),
      Pengangkutan = require('../../../models/Pengangkutan'),
      Util = require('../util/util');

exports.statusPetugas = function (msg,bot,ContextHelper) {
    Petugas.findOne({id_telegram:msg.from.id},"name job")
        .populate('job.tps','name address')
        .exec(function (err,result) {
            if(err){
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
                return;
            }
            if(Util.isEmpty(result)){
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.ERR_BUKAN_PEGAWAI));
                return;
            }
            //bot.sendMessage(msg.from.id,result.toString());
            if(result.job.length > 0){

                let text = TextHelper.getText(Constant.TPS_YG_HRS_DI_AMBIL) + "\n";
                result.job.forEach(function (item,index) {
                    text += "\n" + (index + 1) + " .";
                    text += item.tps.name.bold()+ "\n";
                    text += item.tps.address + "\n";
                });
                bot.sendMessage(msg.from.id,text,{parse_mode:"html"});
            }else{
                bot.sendMessage(msg.from.id,TextHelper.getTextCustom(Constant.NO_JOB_SIR,result.name));
            }
        });
};

exports.akunPetugas = function (msg,bot,ContextHelper) {
    Petugas.findOne({id_telegram:msg.from.id},"name no_hp email tps")
        .populate('tps','name address container')
        .exec(function (err,result) {
       if(err){
           bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
           return;
       }
       if(Util.isEmpty(result)){
           bot.sendMessage(msg.from.id,TextHelper.getText(Constant.ERR_BUKAN_PEGAWAI));
           return;
       }

       let text = emoji.emojify(":bust_in_silhouette: Akun anda\n").bold();
       text += "\nNama : ".bold() + result.name ;
       text += "\nNomor Hp : ".bold() + emoji.get(":iphone:") + " " + result.no_hp;
       text += "\nEmail : ".bold() + emoji.get(":email:") + " " + (result.email || (emoji.get(":no_entry_sign:") + "email"));

       text += "\n\nTotal Tps : ".bold() + result.tps.length + "\n";

       if(result.tps.length>0){
           result.tps.forEach(function (item,index) {
                let tps = "\nTps : ".bold() + item.name;
                tps += "\nAlamat: ".bold() + item.address ;
                tps += "\nKontainer : ".bold();
               if(result.tps[index].container.length>0){
                   let kontainer = "\n";
                   for (let i=0;i<result.tps[index].container.length;i++){
                       kontainer +=  (i+1) + ". " + result.tps[index].container[i].status + "\n";
                   }
                   tps += kontainer;
               }else{
                   tps += emoji.get(":no_entry_sign:") + "kontainer\n" ;
               }
                text += tps;
           });
       }else{
           text += emoji.get(":no_entry_sign:") + "tps";
       }
       bot.sendMessage(msg.from.id,text,{parse_mode:"html"});
    });
};

function savePengankutan(petugas,tps,angkut) {
    Pengangkutan.findOneAndUpdate({_id:angkut},{petugas:petugas,tps:tps},function (err,docs) {
        
    });
}

exports.cekPetugasPengangkutan = function (msg,bot,ContextHelper) {
    let str = msg.text || "";
    str = str.toLowerCase();
    if(str == "sudah" || str == "iya" || str == "ya"){
        Petugas.findOne({id_telegram:msg.from.id}).populate('job.tps','name').exec(function (err,petugas) {
            if(err){
                bot.sendMessage(msg.from.id,TextHelper.getText("DUMMY"));
                return;
            }
            if(Util.isEmpty(petugas)){
                bot.sendMessage(msg.from.id,TextHelper.getText("DUMMY"));
                return;
            }
            if(!Util.isEmpty(petugas)) {
                if (petugas.job.length > 0) {
                    bot.sendMessage(msg.from.id,TextHelper.getText(Constant.PILIH_TPS),Util.getListTpsMarkup(petugas.job));
                } else {
                    bot.sendMessage(msg.from.id, TextHelper.getText(Constant.PEGAWAI_NO_JOB));
                }
            }
        });
    }else{
        bot.sendMessage(msg.from.id,TextHelper.getText("DUMMY"));
    }
};


exports.petugasInlineQuery =function (msg,bot,ContextHelper) {
    Petugas.findOne({id_telegram:msg.from.id},function (err,result) {
       if(err){
           bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
       }else{
           if(!Util.isEmpty(result)){

                if(msg.data.includes("ptgspil")){
                    if(result.job.length==0){
                        bot.answerCallbackQuery(msg.id,"sudah");
                        bot.sendMessage(msg.from.id,TextHelper.getText(Constant.SUDAH_DIAMBIL));
                        return;
                    }
                    let i1,i2;
                    for (let i=0;i<msg.data.length;i++){
                        if(msg.data[i] == "-"){
                            i1 = i+1;
                        }
                        if(msg.data[i] == "_"){
                            i2 = i;
                            break;
                        }
                    }
                    bot.answerCallbackQuery(msg.id,"tps");
                    let id_tps = msg.data.substring(i1,i2);
                    let id_container = msg.data.substring(i2+1,msg.data.length);

                    TPS.findOne({'_id':id_tps,'container._id':id_container},'container',function (er2,hasil) {
                        if(er2){
                            bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
                        }else{
                            if(!Util.isEmpty(hasil)){

                                if(hasil.container.length>0){
                                    for(let i=0;i<hasil.container.length;i++){

                                        if(id_container == hasil.container[i].id){
                                            if(hasil.container[i].status == "KOSONG"){
                                                //simpan;
                                                for(let j=0;j<result.job.length;j++){
                                                    if(result.job[j].container == id_container){
                                                        savePengankutan(result.id,id_tps,result.job[j].angkut);
                                                        result.job.pull(result.job[j].id);
                                                        result.save(function (err) {
                                                            if(err){
                                                                console.log(err);
                                                            }else{
                                                                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.PEGAWAI_SUDAH));
                                                            }
                                                        });
                                                        break;
                                                    }
                                                }


                                            }else{
                                                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.PEGAWAI_LIAR));
                                            }
                                            break;
                                        }

                                    }
                                }

                            }
                        }
                    });
                }

           }else{
               bot.answerCallbackQuery(msg.id,"akses ditolak");
               bot.sendMessage(msg.from.id,TextHelper.getText(Constant.AKSES_DITOLAK));
           }
       }
    });
};