/**
 * Created by muharizals on 01/03/2017.
 */

const Constant = require('../util/Constants'),
      TextHelper = require('../helper/TextHelper'),
      Admin = require('../../../models/Admin'),
      Petugas = require('../../../models/Petugas'),
      BaseController = require('./BaseController'),
      Util = require('../util/util');


function getListAdministrator(ContextHelper,msg,bot,ishapus) {
    Admin.find({id_telegram :{$ne:null}},"username email id_telegram",function (err,result) {
        if(err){
            bot.answerCallbackQuery(msg.id,"error");
            bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
            return;
        }
        if(Util.isEmpty(result)){
            bot.answerCallbackQuery(msg.id,TextHelper.getText(Constant.NO_ADMIN_LIST),true);
            ContextHelper.removeCoversationContext(msg.from.id);
            return;
        }

        bot.answerCallbackQuery(msg.id,"list admin");
        let text = TextHelper.getText(Constant.LIST_ADMIN);
        if(ishapus){
            text = TextHelper.getText(Constant.PILIH_ADMIN_HAPUS);
        }
        bot.editMessageText(text,{chat_id:msg.from.id,inline_message_id:msg.id,message_id:msg.message.message_id,reply_markup:Util.getListAdminMarkUp(result)});

    });
}

function getListPetugas(ContextHelper,msg,bot,ishapus,admin) {
    let query = {
      id_telegram:{$ne:null}
    };
    if(admin.role == "ADMIN"){
        query.wilayah = admin.wilayah;
    }
    Petugas.find(query,"name id_telegram",function (err,result) {
       if(err){
           bot.answerCallbackQuery(msg.id,"error");
           bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
           return;
       }
       if(Util.isEmpty(result)){
           bot.answerCallbackQuery(msg.id,TextHelper.getText(Constant.NO_PETUGAS_LIST),true);
           ContextHelper.removeCoversationContext(msg.from.id);
           return;
       }
       bot.answerCallbackQuery(msg.id,"list petugas");
       let text = TextHelper.getText(Constant.LIST_PETUGAS);
       if(ishapus){
            text = TextHelper.getText(Constant.PILIH_PETUGAS_HAPUS);
       }
       bot.editMessageText(text,{chat_id:msg.from.id,inline_message_id:msg.id,message_id:msg.message.message_id,reply_markup:Util.getListPetugasMarkup(result)});

    });
}


function resetAdmin(msg,bot) {
    Admin.findOne({id_telegram:msg.from.id},"id_telegram",function (err,result) {
       if(err){
           bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
       }else{
               result.id_telegram = undefined;
               result.save(function (err2) {
                   if(err2){
                       bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
                   }else{
                       bot.editMessageText(TextHelper.getText(Constant.SUCCESS_RESET),{chat_id:msg.from.id,inline_message_id:msg.id,message_id:msg.message.message_id,reply_markup:null});
                   }
               });
           }
    });

}

function updateAdminIdTelegram(msg,bot,ContextHelper) {
    Admin.findOneAndUpdate({username:msg.text},{id_telegram:msg.from.id},function (err,result) {
        if(err){
            bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
            ContextHelper.removeCoversationContext(msg.from.id);
        }else{
            bot.sendMessage(msg.from.id,TextHelper.getText(Constant.SUCCESS_UPDATE));
            ContextHelper.removeCoversationContext(msg.from.id);
        }
    });
}

function updateForceIDTelegram(msg,bot,ContextHelper) {
    Admin.findOne({id_telegram:msg.from.id},function (err,result) {
       if(err){
           bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
           ContextHelper.removeCoversationContext(msg.from.id);
           return;
       }
       if(!Util.isEmpty(result)){
               result.id_telegram = undefined;
               result.save(function (errorr) {
                   if(!errorr){
                       updateAdminIdTelegram(msg,bot,ContextHelper);
                   }
               });
       }else{
           updateAdminIdTelegram(msg,bot,ContextHelper);
       }

    });

}

function responseDeleteAdminOrPetugas(msg,bot,ContextHelper,err,result) {
    if(err){
        bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
        ContextHelper.removeCoversationContext(msg.from.id);
    }else{
        result.id_telegram = undefined;
        result.save(function (err2) {
            if(err2){
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
                ContextHelper.removeCoversationContext(msg.from.id);
            }else{
                bot.editMessageText(TextHelper.getText(Constant.SUCCESS_DELETE),{chat_id:msg.from.id,inline_message_id:msg.id,message_id:msg.message.message_id,reply_markup:null});
            }
        });
    }
}

exports.adminInlineQuery = function (msg,bot,ContextHelper) {
    Admin.findOne({id_telegram:msg.from.id},'role wilayah',function (err,result) {
        if(err){
            bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
        }else{
            if(Util.isEmpty(result)){
                ContextHelper.removeCoversationContext(msg.from.id);
                bot.answerCallbackQuery(msg.id,"akses ditolak");
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.AKSES_DITOLAK));
                return;
            }

                if(msg.data.includes("idtelegramadmin") || msg.data.includes("idtelegrampegawai")){
                    let i1,i2;
                    let concon = ContextHelper.getConversationContext(msg.from.id);
                    for (let i=0;i<msg.data.length;i++){
                        if(msg.data[i] == "-"){
                            i1 = i+1;
                        }
                        if(msg.data[i] == "_"){
                            i2 = i;
                            break;
                        }
                    }

                    if(concon && concon.subcontext != null && concon.subcontext == Constant.con_admin_hapus_pilih){
                        bot.answerCallbackQuery(msg.id,"hapus " + msg.data.substring(i2+1,msg.data.length));

                        Admin.findOne({id_telegram:msg.data.substring(i1,i2)},"id_telegram",function (err,result) {
                            responseDeleteAdminOrPetugas(msg,bot,ContextHelper,err,result);
                        });
                        return;
                    }

                    if(concon && concon.subcontext != null && concon.subcontext == Constant.con_admin_hapus_pilih_pegawai){
                        bot.answerCallbackQuery(msg.id,"hapus " + msg.data.substring(i2+1,msg.data.length));
                        Petugas.findOne({id_telegram:msg.data.substring(i1,i2)},"id_telegram",function (err,result) {
                            responseDeleteAdminOrPetugas(msg,bot,ContextHelper,err,result);
                        });
                        return;
                    }

                    bot.answerCallbackQuery(msg.id,msg.data.substring(i2+1,msg.data.length));

                    return;
                }

                switch (msg.data){
                    case Constant.inline_admin_petugas:
                        bot.answerCallbackQuery(msg.id,"petugas");
                        bot.editMessageText(TextHelper.getText(Constant.PILIH_MENU_PEGAWAI),{chat_id:msg.from.id,inline_message_id:msg.id,message_id:msg.message.message_id,reply_markup:Util.getMarkupAdminPegawaiMenu()});
                        break;
                    case Constant.inline_admin_admin:
                        if(result.role == "SUPERADMIN"){
                            bot.answerCallbackQuery(msg.id,"admin");
                            bot.editMessageText(TextHelper.getText(Constant.PILIH_MENU),{chat_id:msg.from.id,inline_message_id:msg.id,message_id:msg.message.message_id,reply_markup:Util.getMarkupAdminMenuAdmin()});
                        }else{
                            ContextHelper.removeCoversationContext(msg.from.id);
                            bot.answerCallbackQuery(msg.id,"akses ditolak");
                            bot.sendMessage(msg.from.id,TextHelper.getText(Constant.AKSES_DITOLAK));
                        }
                        break;
                    case Constant.inline_admin_pengaturan:
                        bot.answerCallbackQuery(msg.id,"pengaturan");
                        bot.editMessageText(TextHelper.getText(Constant.PILIH_MENU_PENGATURAN),{chat_id:msg.from.id,inline_message_id:msg.id,message_id:msg.message.message_id,reply_markup:Util.getMarkupadminPengaturan()});
                        break;
                    case Constant.inline_admin_back_to_main:
                        bot.answerCallbackQuery(msg.id,"main");
                        bot.editMessageText(TextHelper.getText(Constant.PILIH_MENU),{chat_id:msg.from.id,inline_message_id:msg.id,message_id:msg.message.message_id,reply_markup:Util.getMarkupMenuAdmin(result.role)});
                        break;
                    case Constant.inline_admin_back_to_admin:
                        bot.answerCallbackQuery(msg.id,"admin");
                        ContextHelper.removeCoversationContext(msg.from.id);
                        bot.editMessageText(TextHelper.getText(Constant.PILIH_MENU),{chat_id:msg.from.id,inline_message_id:msg.id,message_id:msg.message.message_id,reply_markup:Util.getMarkupAdminMenuAdmin()});
                        break;
                    case Constant.inline_admin_back_to_petugas:
                        bot.answerCallbackQuery(msg.id,"petugas");
                        ContextHelper.removeCoversationContext(msg.from.id);
                        bot.editMessageText(TextHelper.getText(Constant.PILIH_MENU_PEGAWAI),{chat_id:msg.from.id,inline_message_id:msg.id,message_id:msg.message.message_id,reply_markup:Util.getMarkupAdminPegawaiMenu()});
                        break;
                    case Constant.inline_admin_pengaturan_reset:
                        bot.answerCallbackQuery(msg.id,"reset");
                        resetAdmin(msg,bot);
                        break;
                    case Constant.inline_admin_admin_lihat:
                        if(result.role == "SUPERADMIN"){
                            getListAdministrator(ContextHelper,msg,bot);
                        }
                        else{
                            ContextHelper.removeCoversationContext(msg.from.id);
                            bot.answerCallbackQuery(msg.id,"akses ditolak");
                            bot.sendMessage(msg.from.id,TextHelper.getText(Constant.AKSES_DITOLAK));
                        }
                        break;
                    case Constant.inline_admin_admin_tambah:
                        if(result.role != "SUPERADMIN"){
                            ContextHelper.removeCoversationContext(msg.from.id);
                            bot.answerCallbackQuery(msg.id,"akses ditolak");
                            bot.sendMessage(msg.from.id,TextHelper.getText(Constant.AKSES_DITOLAK));
                            return;
                        }
                        bot.answerCallbackQuery(msg.id,"tambah");
                        ContextHelper.setConversationContext(msg.from.id,Constant.con_admin,Constant.con_admin_tambah_admin_request_username);
                        BaseController.processContext(msg,bot,ContextHelper);
                        break;
                    case Constant.inline_admin_admin_hapus:
                        if(result.role != "SUPERADMIN"){
                            ContextHelper.removeCoversationContext(msg.from.id);
                            bot.answerCallbackQuery(msg.id,"akses ditolak");
                            bot.sendMessage(msg.from.id,TextHelper.getText(Constant.AKSES_DITOLAK));
                            return;
                        }
                        bot.answerCallbackQuery(msg.id,"hapus");
                        ContextHelper.setConversationContext(msg.from.id,Constant.con_admin,Constant.con_admin_hapus_pilih);
                        getListAdministrator(ContextHelper,msg,bot,true);
                        break;
                    case Constant.inline_admin_petugas_lihat:
                        getListPetugas(ContextHelper,msg,bot,false,result);
                        break;
                    case Constant.inline_admin_petugas_tambah:
                        bot.answerCallbackQuery(msg.id,"tambah");
                        ContextHelper.setConversationContext(msg.from.id,Constant.con_admin,Constant.con_admin_tambah_pewagai_request_contact);
                        msg.text = Constant.INPUT_CONTACT;
                        BaseController.processContext(msg,bot,ContextHelper);
                        break;
                    case Constant.inline_admin_petugas_hapus:
                        bot.answerCallbackQuery(msg.id,"hapus");
                        ContextHelper.setConversationContext(msg.from.id,Constant.con_admin,Constant.con_admin_hapus_pilih_pegawai);
                        getListPetugas(ContextHelper,msg,bot,true,result);
                        break;

                }

        }
    });
};


exports.showMenuAdministrator = function (msg,bot,ContextHelper) {
    ContextHelper.setConversationContext(msg.from.id,Constant.con_admin,Constant.con_admin_show_menu);
    BaseController.processContext(msg,bot,ContextHelper);
};

exports.superAdmin=function (msg,bot,ContextHelper) {
    Admin.count({id_telegram:msg.from.id},function (err,count) {
        if(err){
            bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
        }else{
            if(count>0){
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.ALREADY_SET_ADMIN));
            }else{
                ContextHelper.setConversationContext(msg.from.id,Constant.con_admin,Constant.con_admin_request_username);
                msg.text = null;
                BaseController.processContext(msg,bot,ContextHelper);
            }
        }
    });

};

exports.superForceAdmin = function (msg,bot,ContextHelper) {
  ContextHelper.setConversationContext(msg.from.id,Constant.con_force_admin,Constant.con_admin_request_username);
  msg.text = null;
  BaseController.processContext(msg,bot,ContextHelper);
};



exports.con_force_admin = function (msg,bot,ContextHelper,sub) {
  switch (sub){
      case Constant.con_admin_request_username:
          if(!msg.text){
              bot.sendMessage(msg.from.id,TextHelper.getText(Constant.INPUT_USERNAME));
          }else{
              Admin.count({username:msg.text},function (err,count) {
                  if(err){
                      bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
                  }else{
                      if(count>0){
                        updateForceIDTelegram(msg,bot,ContextHelper);
                      }else{
                          bot.sendMessage(msg.from.id,TextHelper.getText(Constant.INVALID_USERNAME));
                      }
                  }
              });
          }
          break;
  }
};


exports.con_admin = function (msg,bot,ContextHelper,sub) {
    switch (sub){
        case Constant.con_admin_request_username:
            if(!msg.text){
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.INPUT_USERNAME));
            }else{
                //cek username
                Admin.findOne({username:msg.text},"id_telegram",function (err,result) {
                   if(err){
                       bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
                        return;
                   }
                   if(Util.isEmpty(result)){
                       bot.sendMessage(msg.from.id,TextHelper.getText(Constant.INVALID_USERNAME));
                       return;
                   }
                   if(!result.id_telegram){
                       updateAdminIdTelegram(msg,bot,ContextHelper);
                       return;
                   }
                   bot.sendMessage(msg.from.id,TextHelper.getTextCustom(Constant.ALREADY_SET_TELEGRAM,msg.text));

                });

            }
            break;
        case Constant.con_admin_tambah_admin_request_username:
            if(!msg.text){
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.INPUT_USERNAME));
            }else{
                //cek username
                Admin.findOne({username:msg.text},"id_telegram",function (err,result) {
                    if(err){
                        bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
                        return;
                    }
                    if(Util.isEmpty(result)){
                        bot.sendMessage(msg.from.id,TextHelper.getText(Constant.INVALID_USERNAME));
                        return;
                    }
                    if(!result.id_telegram){
                        ContextHelper.setConversationContext(msg.from.id,Constant.con_admin,Constant.con_admin_tambah_admin_request_contact,msg.text);
                        msg.text = Constant.INPUT_CONTACT;
                        BaseController.processContext(msg,bot,ContextHelper);
                        return;
                    }
                    bot.sendMessage(msg.from.id,TextHelper.getTextCustom(Constant.ALREADY_SET_TELEGRAM,msg.text));

                });

            }
            break;
        case Constant.con_admin_tambah_admin_request_contact:

            if(msg.text == Constant.INPUT_CONTACT){
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.INPUT_CONTACT));
                return;
            }

            if(!msg.contact){
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.INPUT_CONTACT_WOY));
                return;
            }
            if(!msg.contact.user_id){
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.ERR_INVLID_CONTACT));
                return;
            }

            let con = ContextHelper.getConversationContext(msg.from.id);
            Admin.findOneAndUpdate({username:con.data},{id_telegram:msg.contact.user_id},function (err,result) {
                if(err){
                    bot.sendMessage(msg.from.id,TextHelper.getTextCustom(Constant.ERR_ID_ALREADY_USER,con.data));
                    bot.sendMessage(msg.from.id,TextHelper.getText(Constant.INPUT_CONTACT_LAIN));
                }else{
                    bot.sendMessage(msg.from.id,TextHelper.getText(Constant.SUCCESS_UPDATE));
                    ContextHelper.removeCoversationContext(msg.from.id);
                }
            });

            break;
        case Constant.con_admin_tambah_pewagai_request_contact:
            if(msg.text == Constant.INPUT_CONTACT){
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.INPUT_CONTACT));
                return;
            }

            if(!msg.contact){
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.INPUT_CONTACT_WOY));
                return;
            }
            if(!msg.contact.user_id){
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.ERR_INVLID_CONTACT));
                return;
            }

            Petugas.findOneAndUpdate({no_hp:msg.contact.phone_number},{id_telegram:msg.contact.user_id},function (err,result) {
               if(err){
                   console.log('error');
                   ContextHelper.removeCoversationContext(msg.from.id);
               }else{
                   if(Util.isEmpty(result)){
                        bot.sendMessage(msg.from.id,TextHelper.getText(Constant.INVALID_PHONE_NUMBER));
                        bot.sendMessage(msg.from.id,TextHelper.getText(Constant.INPUT_CONTACT));
                        return;
                   }
                   bot.sendMessage(msg.from.id,TextHelper.getText(Constant.SUCCESS_UPDATE));
                   ContextHelper.removeCoversationContext(msg.from.id);
               }
            });

            break;

        case Constant.con_admin_show_menu:
            Admin.findOne({id_telegram:msg.from.id},'role',function (err,result) {
                if(err){
                    bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
                }
                if(Util.isEmpty(result)){
                    ContextHelper.removeCoversationContext(msg.from.id);
                    bot.sendMessage(msg.from.id,TextHelper.getText(Constant.AKSES_DITOLAK));
                    return;
                }
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.PILIH_MENU),Util.getMarkupAdminMenu(result.role));
                ContextHelper.removeCoversationContext(msg.from.id);

            });
            /*Admin.count({id_telegram:msg.from.id},function (err,count) {
                if(err){
                    bot.sendMessage(msg.from.id,TextHelper.getText(Constant.UNKNOWN_ERROR));
                }else{
                    if(count>0){
                        bot.sendMessage(msg.from.id,TextHelper.getText(Constant.PILIH_MENU),Util.getMarkupAdminMenu());
                        ContextHelper.removeCoversationContext(msg.from.id);
                    }else{
                        ContextHelper.removeCoversationContext(msg.from.id);
                        bot.sendMessage(msg.from.id,TextHelper.getText(Constant.AKSES_DITOLAK));
                    }
                }
            });*/
            break;

    }
};

