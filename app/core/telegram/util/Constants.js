/**
 * Created by muharizals on 01/03/2017.
 */

module.exports = {
    WELCOME: "WELCOME",

    NO_CONTEXT : "NO_CONTEXT",
    PERINTAH_DIBATALKAN: "PERINTAH_DIBATALKAN",
    INPUT_USERNAME : "INPUT_USERNAME",
    INPUT_CONTACT : "INPUT_CONTACT",
    INPUT_CONTACT_WOY: "INPUT_CONTACT_WOY",
    INPUT_CONTACT_LAIN: "INPUT_CONTACT_LAIN",

    NO_JOB_SIR : "NO_JOB_SIR",

    TPS_YG_HRS_DI_AMBIL : "TPS_YG_HRS_DI_AMBIL",

    PILIH_TPS : "pilih_TPS",
    PILIH_MENU : "pilih_menu",
    PILIH_MENU_PEGAWAI: "PILIH_MENU_PEGAWAI",
    PILIH_MENU_PENGATURAN: "PILIH_MENU_PENGATURAN",
    PILIH_ADMIN_HAPUS: "PILIH_ADMIN_HAPUS",
    PILIH_PETUGAS_HAPUS: "PILIH_PETUGAS_HAPUS",

    LIST_ADMIN : "LIST_ADMIN",
    LIST_PETUGAS : "LIST_PETUGAS",

    ALREADY_SET_ADMIN : "ALREADY_SET_ADMIN",
    AKSES_DITOLAK : "AKSES_DITOLAK",
    ALREADY_SET_TELEGRAM: "ALREADY_SET_TELEGRAM",

    UNKNOWN_ERROR: "UNKNOWN_ERROR",

    NO_ADMIN_LIST : "NO_ADMIN_LIST",
    NO_PETUGAS_LIST: "NO_PETUGAS_LIST",

    INVALID_USERNAME: "INVALID_USERNAME",
    INVALID_PHONE_NUMBER: "INVALID_PHONE_NUMBER",

    ERR_BUKAN_PEGAWAI : "ERR_BUKAN_PEGAWAI",
    ERR_ID_ALREADY_USER : "ERR_ID_ALREADY_USER",
    ERR_INVLID_CONTACT: "ERR_INVLID_CONTACT",

    SUCCESS_UPDATE : "SUCCESS_UPDATE",
    SUCCESS_RESET: "SUCCESS_RESET",
    SUCCESS_DELETE: "SUCCESS_DELETE",

    SUDAH_DIAMBIL: "SUDAH_DIAMBIL",

    PEGAWAI_NO_JOB : "PEGAWAI_NO_JOB",
    PEGAWAI_LIAR : "PEGAWAI_LIAR",
    PEGAWAI_SUDAH : "PEGAWAI_SUDAH",

    con_admin: "con_admin",
    con_admin_request_username: "con_admin_request_username",
    con_admin_show_menu: "con_admin_show_menu",
    con_admin_pegawai_menu: "con_admin_pegawai_menu",

    con_admin_hapus_pilih: "con_admin_hapus_pilih",
    con_admin_hapus_pilih_pegawai: "con_admin_hapus_pilih_pegawai",

    con_admin_tambah_admin_request_username: "con_admin_tambah_admin",
    con_admin_tambah_admin_request_contact : "con_admin_tambah_admin_request_contact",

    con_admin_tambah_pewagai_request_contact : "con_admin_tambah_pewagai_request_contact",

    con_force_admin: "con_force_admin",



    inline_admin_petugas: "inline_admin_petugas",
    inline_admin_admin: "inline_admin_admin",
    inline_admin_pengaturan: "inline_admin_pengaturan",

    inline_admin_petugas_lihat : "inline_admin_petugas_lihat",
    inline_admin_petugas_tambah: "inline_admin_petugas_tambah",
    inline_admin_petugas_hapus: "inline_admin_petugas_hapus",

    inline_admin_back_to_main: "inline_admin_back_to_main",
    inline_admin_back_to_admin : "inline_admin_back_to_admin",
    inline_admin_back_to_petugas : "inline_admin_back_to_petugas",

    inline_admin_pengaturan_reset: "inline_admin_pengaturan_reset",

    inline_admin_list_admin : "inline_admin_list_admin",

    inline_admin_admin_lihat : "inline_admin_admin_lihat",
    inline_admin_admin_tambah: "inline_admin_admin_tambah",
    inline_admin_admin_hapus: "inline_admin_admin_hapus"
};