/**
 * Created by muharizals on 01/03/2017.
 */
const Constant = require('./Constants');

exports.getMarkupMenuAdmin=function(role){
    let isi_markup = [];
    if(role == "SUPERADMIN"){
       isi_markup = [{ text: 'Petugas', callback_data: Constant.inline_admin_petugas },{ text: 'Admin', callback_data: Constant.inline_admin_admin },{ text: 'Pengaturan', callback_data: Constant.inline_admin_pengaturan }];
    }
    else {
        isi_markup = [{ text: 'Petugas', callback_data: Constant.inline_admin_petugas },{ text: 'Pengaturan', callback_data: Constant.inline_admin_pengaturan }];
    }
    return JSON.stringify({
        inline_keyboard: [
            isi_markup
        ]
    })
};

exports.getMarkupAdminMenu = function (role) {
    return {
        reply_markup: this.getMarkupMenuAdmin(role)
    };

};

function getTpsList(list) {
    let inline_keyboard = [];
    list.forEach(function (item) {
        let childitem = [];
        let callback = "ptgspil-" +  item.tps.id + "_"+item.container;
        childitem.push({text:item.tps.name,callback_data:callback});
        inline_keyboard.push(childitem);
    });

    return JSON.stringify({
        inline_keyboard:inline_keyboard
    });
}

exports.getListTpsMarkup = function (list) {
    return {
        reply_markup: getTpsList(list)
    };
};

exports.getListAdminMarkUp= function (list) {
    let inline_keyboard = [];
    let childitem = [];
    list.forEach(function (item,index) {
        let callback = "idtelegramadmin-" + item.id_telegram + "_" + item.email;
        childitem.push({text:item.username,callback_data:callback});
        if(childitem.length == 2){
            inline_keyboard.push(childitem);
            childitem = [];
        }
        if(index == list.length-1){
            childitem.push({text:'« Kembali',callback_data:Constant.inline_admin_back_to_admin});
        }
    });
    inline_keyboard.push(childitem);
    return JSON.stringify({
        inline_keyboard:inline_keyboard
    });
};

exports.getListPetugasMarkup = function (list) {
    let inline_keyboard = [];
    let childitem = [];
    list.forEach(function (item,index) {
       let callback = "idtelegrampegawai-" + item.id_telegram + "_" + item.name;
       childitem.push({text:item.name,callback_data:callback});
        if(childitem.length == 2){
            inline_keyboard.push(childitem);
            childitem = [];
        }
        if(index == list.length-1){
            childitem.push({text:'« Kembali',callback_data:Constant.inline_admin_back_to_petugas});
        }
    });
    inline_keyboard.push(childitem);
    return JSON.stringify({
        inline_keyboard:inline_keyboard
    });
};

exports.getMarkupAdminPegawaiMenu = function () {
  return JSON.stringify({
          inline_keyboard: [
            [{text:'List Petugas',callback_data:Constant.inline_admin_petugas_lihat}],
             [{text:'Tambah Petugas',callback_data:Constant.inline_admin_petugas_tambah}],
             [{text:'Hapus Petugas',callback_data:Constant.inline_admin_petugas_hapus}],
              [{text:'« Kembali',callback_data:Constant.inline_admin_back_to_main}]
         ]
      });

};

exports.getMarkupadminPengaturan =function () {
  return JSON.stringify({
     inline_keyboard: [
         [{text:'Reset',callback_data:Constant.inline_admin_pengaturan_reset}],
         [{text:'« Kembali',callback_data:Constant.inline_admin_back_to_main}]
     ]
  });
};

exports.getMarkupAdminMenuAdmin = function(){
    return JSON.stringify({
            inline_keyboard: [
                [{text:'List Admin',callback_data:Constant.inline_admin_admin_lihat}],
                [{text:'Tambah Admin',callback_data:Constant.inline_admin_admin_tambah}],
                [{text:'Hapus Admin',callback_data:Constant.inline_admin_admin_hapus}],
                [{text:'« Kembali',callback_data:Constant.inline_admin_back_to_main}]
            ]
        });
};

exports.isEmpty=function(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
};