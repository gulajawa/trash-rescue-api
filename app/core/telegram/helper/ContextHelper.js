/**
 * Created by muharizals on 28/02/2017.
 */

var listofUser =[];

function cekById(id){
    for (let i=0;i<listofUser.length;i++){
        if(listofUser[i].id == id){
            return listofUser[i];
        }
    }
    return null;
}

exports.getConversationContext = function (id) {
    let res =cekById(id);
    if(res == null){
        return null;
    }
    return res;
};

exports.removeCoversationContext = function (id) {
    for (let i=0;i<listofUser.length;i++){
        if(listofUser[i].id == id){
            listofUser.splice(i,1);
            break;
        }
    }
};

exports.setConversationContext = function (id,context,subcontext,data) {
    if(listofUser.length == 0 || cekById(id) == null){
        let conversation = {
            id:id,
            context:context
        };
        if(subcontext){
            conversation.subcontext = subcontext;
        }
        if(data){
            conversation.data = data;
        }
         listofUser.push(conversation);
         return;
    }
    if(cekById(id) != null){
        for(let i=0;i<listofUser.length;i++){
            if(listofUser[i].id == id){
                if(context){
                    listofUser[i].context =context;
                }
                if(subcontext){
                    listofUser[i].subcontext = subcontext;
                }
                if(data){
                    listofUser[i].data = data;
                }
                break;
            }
        }

    }

};