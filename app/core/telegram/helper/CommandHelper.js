/**
 * Created by muharizals on 28/02/2017.
 */

const listCommand = [
    "/force_super_admin_321",
    "/superadmin123",
    "/akun",
    "/batal",
    "/start",
    "/admin",
    "/status"
];

const Constant = require('../util/Constants'),
      TextHelper = require('./TextHelper'),
      AdminController = require('../controller/AdminController'),
      PetugasController = require('../controller/PetugasController');

exports.isCommands = function (text) {
    if(!text){
        return false;
    }
    text = text.toLowerCase();
    for(let i=0;i<listCommand.length;i++){
        if(text == listCommand[i]){
            return true;
        }
    }
    return false;
};

exports.processCommand = function (msg,bot,ContextHelper) {
    let text = msg.text;text = text.toLowerCase();
    switch (text){
       case listCommand[0]:
            AdminController.superForceAdmin(msg,bot,ContextHelper);
            break;
       case listCommand[1]:
            AdminController.superAdmin(msg,bot,ContextHelper);
           break;
       case listCommand[2]:
            PetugasController.akunPetugas(msg,bot,ContextHelper);
           break;
       case listCommand[3]:
           if(ContextHelper.getConversationContext(msg.from.id) == null){
                bot.sendMessage(msg.from.id,TextHelper.getText(Constant.NO_CONTEXT));
                return;
           }
           ContextHelper.removeCoversationContext(msg.from.id);
           bot.sendMessage(msg.from.id,TextHelper.getText(Constant.PERINTAH_DIBATALKAN));
           break;
       case listCommand[4]:
            bot.sendMessage(msg.from.id,TextHelper.getTextCustom(Constant.WELCOME,msg.from.first_name));
            break;
       case listCommand[5]:
            AdminController.showMenuAdministrator(msg,bot,ContextHelper);
           break;
        case listCommand[6]:
            PetugasController.statusPetugas(msg,bot,ContextHelper);
            break;
   }
};