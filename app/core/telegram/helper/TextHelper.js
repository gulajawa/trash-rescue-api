/**
 * Created by muharizals on 28/02/2017.
 */


const emoji = require('node-emoji'),
      Constant = require('../util/Constants');


function TextHelper() {
    this.getText=function (id) {
        let text = "";
        switch(id){
            case Constant.PEGAWAI_SUDAH:
                text = emoji.emojify("Mantap :grimacing: , semangat kerjanya pak :heart_eyes: :kissing_heart: ");
                break;
            case Constant.PEGAWAI_LIAR:
                text = emoji.emojify("Kontainer masih penuh dan belum diambil :disappointed_relieved: :disappointed_relieved:");
                break;
            case Constant.PEGAWAI_NO_JOB:
                text = emoji.emojify("Tidak ada kontainer yang harus di ambil :sweat_smile:  Zzzzz ...");
                break;
            case Constant.NO_CONTEXT:
                text = emoji.emojify("Tidak ada perintah aktif untuk dibatalkan :sweat_smile: . Saya bahkan tidak melakukan apa pun. Zzzzz ...");
                break;
            case Constant.SUDAH_DIAMBIL:
                text = emoji.emojify("Kontainer ini sudah di ambil :sweat_smile: .Zzzzz ...");
                break;
            case Constant.PERINTAH_DIBATALKAN:
                text = emoji.emojify("perintah dibatalkan :smile: ");
                break;
            case Constant.INPUT_USERNAME:
                text = "Masukan username administrator ";
                break;
            case Constant.INPUT_CONTACT:
                text = "Kirimkan kontak";
                break;
            case Constant.INPUT_CONTACT_LAIN:
                text = "Kirimkan kontak lainya";
                break;
            case Constant.INPUT_CONTACT_WOY:
                text = emoji.emojify("Anda tidak mengirimkan kontak :pensive: :pensive:");
                break;
            case Constant.UNKNOWN_ERROR:
                text = emoji.emojify("Terjadi kesahalan yang tidak diketahui :worried: :worried: ");
                break;
            case Constant.INVALID_USERNAME:
                text = emoji.emojify("Username administrator tidak ditemukan :confused: , coba lagi");
                break;
            case Constant.INVALID_PHONE_NUMBER:
                text = emoji.emojify("Kontak pegawai tidak ditemukan :unamused: ");
                break;
            case Constant.SUCCESS_UPDATE:
                text = emoji.emojify(":raised_hands: :raised_hands: data berhasil diperbaruhui");break;
            case Constant.SUCCESS_DELETE :
                text = emoji.emojify(":raised_hands: :raised_hands: data berhasil dihapus");break;
            case Constant.ERR_ID_ALREADY_USER:
                text = emoji.emojify("Akun anda sudah di setting sebagai administrator :open_mouth: ");break;
            case Constant.ERR_INVLID_CONTACT:
                text = emoji.emojify("Kontak tidak terdaftar di telegram :disappointed: ");break;
            case Constant.ERR_BUKAN_PEGAWAI:
                text = emoji.emojify("Sepertinya anda tidak terdaftar di akun kami :sob: :sob: ");
                break;
            case Constant.ALREADY_SET_ADMIN:
                text = "Anda sudah menjadi administator";break;
            case Constant.PILIH_MENU:
                text = "pilih menu administrator";break;
            case Constant.PILIH_TPS:
                text = "pilih satu tps yang telah di proses";
                break;
            case Constant.PILIH_MENU_PEGAWAI:
                text = "pilih menu pegawai";break;
            case Constant.PILIH_MENU_PENGATURAN:
                text = "pilih menu pengaturan";break;
            case Constant.PILIH_ADMIN_HAPUS:
                text = "pilih admin untuk dihapus";
                break;
            case Constant.PILIH_PETUGAS_HAPUS:
                text = "pilih petugas untuk dihapus";
                break;
            case Constant.SUCCESS_RESET :
                text = emoji.emojify("berhasil direset :raised_hands: :raised_hands:");
                break;
            case Constant.AKSES_DITOLAK:
                text = emoji.emojify("Sepertinya anda bukan seorang administrator :confused: ");
                break;
            case Constant.NO_ADMIN_LIST:
                text = emoji.emojify("tidak ada list admin :frowning: ");
                break;
            case Constant.NO_PETUGAS_LIST:
                text = emoji.emojify("tidak ada list petugas :frowning: ");
                break;
            case Constant.LIST_ADMIN:
                text = "list admin";break;
            case Constant.LIST_PETUGAS:
                text = "list petugas";break;
            case Constant.TPS_YG_HRS_DI_AMBIL :
                text = "Berikut list tps yang harus di ambil : ";
                break;
            default:
                text = emoji.emojify("hai :smile: :smile: ");break;
        }
        return text;
    };

    this.getTextCustom=function (id,text) {
        let tr = "";
        switch (id){
            case Constant.ALREADY_SET_TELEGRAM:
                text = "Akun " + text + " sudah di digunakan oleh orang lain. :worried: coba lagi :smile:";
                tr = emoji.emojify(text);
                break;
            case Constant.WELCOME :
                text = "Hai "+ text + " :blush: , Selamat datang di Trash Rescue, saya adalah robot yang membantu petugas kebersihan untuk memberi notifikasi kalau kontainer sudah penuh :grinning: ";
                tr = emoji.emojify(text);
                break;
            case Constant.ERR_ID_ALREADY_USER:
                text = "Akun " + text + " sudah di setting sebagai administrator :open_mouth:";
                tr = emoji.emojify(text);break;
            case Constant.NO_JOB_SIR :
                text = "Hai " + text + ", tidak ada tps yang harus anda ambil untuk saat ini, terimakasih.:smile: :smile:";
                tr = emoji.emojify(text);break;
        }
        return tr;
    };
}

module.exports = new TextHelper();